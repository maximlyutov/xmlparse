package alcoportal.dal.domain.entity.userInfo;

import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.entity.UserEntity;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;

/**
 * Created by good on 29.10.2015.
 */
@Entity
@Table(name = "license")
@SequenceGenerator(sequenceName = "license_sequence", name = "default_generator", allocationSize = 1)
public class LicenseEntity extends BaseEntity {

    @ManyToOne(targetEntity = SubdivisionEntity.class)
    private SubdivisionEntity subdivision;

    @OneToOne(targetEntity = UserEntity.class)
    private UserEntity user;

    @Column(name = "work_type")
    private String workType;
    @Column(name = "date_start")
    private String dateStart;
    @Column(name = "date_end")
    private String dateEnd;
    private String number;
    @Column(name = "number_blank")
    private String numberBlank;

    public SubdivisionEntity getSubdivision() {
        return subdivision;
    }

    public void setSubdivision(SubdivisionEntity subdivision) {
        this.subdivision = subdivision;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumberBlank() {
        return numberBlank;
    }

    public void setNumberBlank(String numberBlank) {
        this.numberBlank = numberBlank;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
