package alcoportal.dal.domain.entity;

import alcoportal.dal.domain.other.RoleEnum;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by good on 03.10.2015.
 */
@Entity(name = "roles")
@SequenceGenerator(sequenceName = "roles_sequence", name = "default_generator", allocationSize = 1)
public class UserRoleEntity extends BaseEntity{

    @Column(name = "title", nullable = false, unique = true)
    @Enumerated(value = EnumType.STRING)
    private RoleEnum title;

    public UserRoleEntity() {
    }

    public RoleEnum getTitle() {
        return title;
    }

    public void setTitle(RoleEnum title) {
        this.title = title;
    }
}