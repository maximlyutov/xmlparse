package alcoportal.dal.domain.entity.D6;

import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vladimir akummail@gmail.com on 10/21/15.
 */
@Entity
@Table(name = "kontragents")
@SequenceGenerator(sequenceName = "kontragents_sequence", name = "default_generator", allocationSize = 1)
public class KontragentEntity extends BaseEntity {

    String inn;
    String kpp;
    String licenseNumber;

    Date startDateLicensing;
    Date expirationDateLicense;

    @OneToOne(optional = false)
    @JoinColumn(name = "document_history_id")
    DocumentHistoryEntity documentHistoryEntity;

    public DocumentHistoryEntity getDocumentHistoryEntity() {
        return documentHistoryEntity;
    }

    public KontragentEntity setDocumentHistoryEntity(DocumentHistoryEntity documentHistoryEntity) {
        this.documentHistoryEntity = documentHistoryEntity;
        return this;
    }

    public String getInn() {
        return inn;
    }

    public KontragentEntity setInn(String inn) {
        this.inn = inn;
        return this;
    }

    public String getKpp() {
        return kpp;
    }

    public KontragentEntity setKpp(String kpp) {
        this.kpp = kpp;
        return this;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public KontragentEntity setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
        return this;
    }

    public Date getStartDateLicensing() {
        return startDateLicensing;
    }

    public KontragentEntity setStartDateLicensing(Date startDateLicensing) {
        this.startDateLicensing = startDateLicensing;
        return this;
    }

    public Date getExpirationDateLicense() {
        return expirationDateLicense;
    }

    public KontragentEntity setExpirationDateLicense(Date expirationDateLicense) {
        this.expirationDateLicense = expirationDateLicense;
        return this;
    }

    @Override
    public String toString() {

        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");

        return "KontragentEntity{" +
                " Инн Контарента='" + inn + '\'' +
                ", Кпп Контрагента='" + kpp + '\'' +
                ", Регистрационный номер лицензии='" + licenseNumber + '\'' +
                ", Дата выдачи лицензии=" + df.format(startDateLicensing) +
                ", Дата окончания лицензии=" + df.format(expirationDateLicense) +
                '}';
    }
}
