package alcoportal.dal.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by vladimir akummail@gmail.com on 10/18/15.
 */
@Entity
@Table(name = "validation_errors")
@SequenceGenerator(sequenceName = "validation_errors_sequence", name = "default_generator", allocationSize = 1)
public class ValidationErrorEntity extends BaseEntity {

    @Column(name = "form_name")
    String formName;
    @Column(name = "element_name")
    String elementName;
    @Column(name = "description")
    String description;

    public String getFormName() {
        return formName;
    }

    public ValidationErrorEntity setFormName(String formName) {
        this.formName = formName;
        return this;
    }

    public String getElementName() {
        return elementName;
    }

    public ValidationErrorEntity setElementName(String elementName) {
        this.elementName = elementName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ValidationErrorEntity setDescription(String description) {
        this.description = description;
        return this;
    }
}
