package alcoportal.dal.domain.entity.revise;

/**
 * Created by vladimir akummail@gmail.com on 11/3/15.
 */
public enum ReviseState {
    IN_PROGRESS,
    ERROR,
    SUCCESS;

    @Override
    public String toString() {
        switch (this) {
            case IN_PROGRESS:
                return "В процессе";
            case ERROR:
                return "Ошибка сверки";
            case SUCCESS:
                return "Сверено";
            default:
                return "";
        }
    }
}
