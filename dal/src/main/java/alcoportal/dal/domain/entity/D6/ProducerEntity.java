package alcoportal.dal.domain.entity.D6;

import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;

import javax.persistence.*;

/**
 * Created by vladimir akummail@gmail.com on 10/21/15.
 */
@Entity
@Table(name = "producers")
@SequenceGenerator(sequenceName = "producers_sequence", name = "default_generator", allocationSize = 1)
public class ProducerEntity extends BaseEntity {

    String inn;
    String kpp;
    @OneToOne(optional = false)
    @JoinColumn(name = "document_history_id")
    DocumentHistoryEntity documentHistory;

    public DocumentHistoryEntity getDocumentHistory() {
        return documentHistory;
    }

    public ProducerEntity setDocumentHistory(DocumentHistoryEntity documentHistory) {
        this.documentHistory = documentHistory;
        return this;
    }

    public String getInn() {
        return inn;
    }

    public ProducerEntity setInn(String inn) {
        this.inn = inn;
        return this;
    }

    public String getKpp() {
        return kpp;
    }

    public ProducerEntity setKpp(String kpp) {
        this.kpp = kpp;
        return this;
    }

    @Override
    public String toString() {
        return "ProducerEntity{" +
                " Инн Производителя='" + inn + '\'' +
                ", КПП Производителя='" + kpp + '\'' +
                '}';
    }
}
