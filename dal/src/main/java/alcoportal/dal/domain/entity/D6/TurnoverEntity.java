package alcoportal.dal.domain.entity.D6;

import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.other.TurnoverType;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vladimir akummail@gmail.com on 10/21/15.
 */
@Entity
@Table(name = "turnovers")
@SequenceGenerator(sequenceName = "turnovers_sequence", name = "default_generator", allocationSize = 1)
public class TurnoverEntity extends BaseEntity {
    String productCode;
    Date purchaseDate;

    @Column(name = "number_ttn")
    String numberTTN;
    @Column(name = "number_gtd")
    String numberGTD;
    double volume;

    String kpp;

    @Enumerated(EnumType.ORDINAL)
    TurnoverType type;

    @OneToOne(optional = false)
    @JoinColumn(name = "document_history_id")
    DocumentHistoryEntity documentHistory;


    @OneToOne(optional = false)
    @JoinColumn(name = "kontragent_id")
    KontragentEntity kontragent;

    public String getKpp() {
        return kpp;
    }

    public TurnoverEntity setKpp(String kpp) {
        this.kpp = kpp;
        return this;
    }

    public KontragentEntity getKontragent() {
        return kontragent;
    }

    public TurnoverEntity setKontragent(KontragentEntity kontragent) {
        this.kontragent = kontragent;
        return this;
    }

    @OneToOne(optional = false)
    @JoinColumn(name = "producer_id")
    ProducerEntity producer;

    public ProducerEntity getProducer() {
        return producer;
    }

    public TurnoverEntity setProducer(ProducerEntity producer) {
        this.producer = producer;
        return this;
    }

    public DocumentHistoryEntity getDocumentHistory() {
        return documentHistory;
    }

    public TurnoverEntity setDocumentHistory(DocumentHistoryEntity documentHistory) {
        this.documentHistory = documentHistory;
        return this;
    }

    public TurnoverType getType() {
        return type;
    }

    public TurnoverEntity setType(TurnoverType type) {
        this.type = type;
        return this;
    }

    public String getProductCode() {
        return productCode;
    }

    public TurnoverEntity setProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public TurnoverEntity setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
        return this;
    }

    public String getNumberTTN() {
        return numberTTN;
    }

    public TurnoverEntity setNumberTTN(String numberTTN) {
        this.numberTTN = numberTTN;
        return this;
    }

    public String getNumberGTD() {
        return numberGTD;
    }

    public TurnoverEntity setNumberGTD(String numberGTD) {
        this.numberGTD = numberGTD;
        return this;
    }

    public double getVolume() {
        return volume;
    }

    public TurnoverEntity setVolume(double volume) {
        this.volume = volume;
        return this;
    }

    @Override
    public String toString() {
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        return "TurnoverEntity{" +
                " Код вида продукции ='" + productCode + '\'' +
                ", Дата закупки =" + df.format(purchaseDate) +
                ", Номер ТТН ='" + numberTTN + '\'' +
                ", Номер ГТД ='" + numberGTD + '\'' +
                ", Объем продукции ='" + volume + '\'' +
                '}';
    }
}
