package alcoportal.dal.domain.entity;

import javax.persistence.*;

/**
 * Created by vladimir akummail@gmail.com on 11/21/15.
 */
@Entity
@Table(name = "turnovers_d5")
@SequenceGenerator(sequenceName = "turnovers_d5_sequence", name = "default_generator", allocationSize = 1)
public class TurnoverD5 extends BaseEntity {

    String inn;

    String kpp;

    String productCode;

    @OneToOne(optional = false)
    @JoinColumn(name = "document_history_id")
    DocumentHistoryEntity documentHistory;

    @Column(name = "release_volume")
    double releaseVolume;

    @Column(name = "release_return_kontragent_volume")
    double releaseReturnKontragentVolume;

    @Column(name = "release_other_volume")
    double releaseOtherVolume;

    @Column(name = "consumption_volume")
    double consumptionVolume;

    @Column(name = "consumption_other_volume")
    double consumptionOherVolume;

    @Column(name = "consumption_return_kontragent_volume")
    double consumptionReturnKontragentVolume;


    public String getInn() {
        return inn;
    }

    public TurnoverD5 setInn(String inn) {
        this.inn = inn;
        return this;
    }

    public String getKpp() {
        return kpp;
    }

    public TurnoverD5 setKpp(String kpp) {
        this.kpp = kpp;
        return this;
    }

    public String getProductCode() {
        return productCode;
    }

    public TurnoverD5 setProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public DocumentHistoryEntity getDocumentHistory() {
        return documentHistory;
    }

    public TurnoverD5 setDocumentHistory(DocumentHistoryEntity documentHistory) {
        this.documentHistory = documentHistory;
        return this;
    }

    public double getReleaseVolume() {
        return releaseVolume;
    }

    public TurnoverD5 setReleaseVolume(double releaseVolume) {
        this.releaseVolume = releaseVolume;
        return this;
    }

    public double getReleaseReturnKontragentVolume() {
        return releaseReturnKontragentVolume;
    }

    public TurnoverD5 setReleaseReturnKontragentVolume(double releaseReturnKontragentVolume) {
        this.releaseReturnKontragentVolume = releaseReturnKontragentVolume;
        return this;
    }

    public double getReleaseOtherVolume() {
        return releaseOtherVolume;
    }

    public TurnoverD5 setReleaseOtherVolume(double releaseOtherVolume) {
        this.releaseOtherVolume = releaseOtherVolume;
        return this;
    }

    public double getConsumptionVolume() {
        return consumptionVolume;
    }

    public TurnoverD5 setConsumptionVolume(double consumptionVolume) {
        this.consumptionVolume = consumptionVolume;
        return this;
    }

    public double getConsumptionOherVolume() {
        return consumptionOherVolume;
    }

    public TurnoverD5 setConsumptionOherVolume(double consumptionOherVolume) {
        this.consumptionOherVolume = consumptionOherVolume;
        return this;
    }

    public double getConsumptionReturnKontragentVolume() {
        return consumptionReturnKontragentVolume;
    }

    public TurnoverD5 setConsumptionReturnKontragentVolume(double consumptionReturnKontragentVolume) {
        this.consumptionReturnKontragentVolume = consumptionReturnKontragentVolume;
        return this;
    }
}
