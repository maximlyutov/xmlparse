package alcoportal.dal.domain.entity.userInfo;

import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.entity.UserEntity;

import javax.persistence.*;

/**
 * Created by good on 29.10.2015.
 */
@Entity
@Table(name = "organisation")
@SequenceGenerator(sequenceName = "organisation_sequence", name = "default_generator", allocationSize = 1)
public class OrganisationEntity extends BaseEntity {
    private String name;
    private Long kpp;
    @Column(name = "fio_dir")
    private String fioDir;
    @Column(name = "fio_buh")
    private String fioBuh;
    @Column(name = "subject_code")
    private String subjectCode;
    @Column(name = "ur_address")
    private String address;
    private String email;
    @Column(name = "org_type")
    private String orgType;

    @OneToOne(targetEntity = UserEntity.class)
    private UserEntity user;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getKpp() {
        return kpp;
    }

    public void setKpp(Long kpp) {
        this.kpp = kpp;
    }

    public String getFioDir() {
        return fioDir;
    }

    public void setFioDir(String fioDir) {
        this.fioDir = fioDir;
    }

    public String getFioBuh() {
        return fioBuh;
    }

    public void setFioBuh(String fioBuh) {
        this.fioBuh = fioBuh;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }
}
