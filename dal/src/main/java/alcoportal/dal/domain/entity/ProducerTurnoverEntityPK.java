package alcoportal.dal.domain.entity;


import javax.persistence.Cacheable;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by vladimir akummail@gmail.com on 11/22/15.
 */
@Cacheable(false)
@Embeddable
public class ProducerTurnoverEntityPK implements Serializable {
    double id;
    String inn;
    String kpp;

    public String getInn() {
        return inn;
    }

    public ProducerTurnoverEntityPK setInn(String inn) {
        this.inn = inn;
        return this;
    }

    public String getKpp() {
        return kpp;
    }

    public ProducerTurnoverEntityPK setKpp(String kpp) {
        this.kpp = kpp;
        return this;
    }

    public double getId() {
        return id;
    }

    public ProducerTurnoverEntityPK setId(double id) {
        this.id = id;
        return this;
    }
}
