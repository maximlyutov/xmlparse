package alcoportal.dal.domain.entity.revise;

import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;

import javax.persistence.*;

/**
 * Created by vladimir akummail@gmail.com on 11/21/15.
 */
@Entity
@Table(name = "revise_d5_logs")
@SequenceGenerator(sequenceName = "revise_d5_logs_sequence", name = "default_generator", allocationSize = 1)
public class ReviseD5LogsEntity extends BaseEntity {

    @OneToOne(optional = false)
    @JoinColumn(name = "revise_id")
    ReviseEntity revise;

    @Column(name = "product_code")
    String productCode;

    @Column(name = "producer_inn")
    String producerInn;

    @Column(name = "producer_kpp")
    String producerKpp;

    @Column(name = "release_d6_volume")
    Double releaseD6Volume;
    @Column(name = "release_return_d6_volume")
    Double releaseReturnD6Volume;

    @Column(name = "release_volume")
    Double releaseVolume;

    @Column(name = "release_return_kontragent_volume")
    Double releaseReturnKontragentVolume;

    @Column(name = "release_other_volume")
    Double releaseOtherVolume;

    @Column(name = "consumption_other_volume")
    Double consumptionOherVolume;

    @Column(name = "consumption_return_kontragent_volume")
    Double consumptionReturnKontragentVolume;

    @Column(name = "release_delta")
    Double releaseDelta;

    @Column(name = "consumption_volume")
    Double consumptionVolume;

    @Column(name = "consumption_d7_volume")
    Double consumptionD7Volume;

    @Column(name = "consumption_return_d7_volume")
    Double consumptionReturnD7Volume;

    @Column(name = "consumption_delta")
    Double consumptionDelta;

    public ReviseEntity getRevise() {
        return revise;
    }

    public ReviseD5LogsEntity setRevise(ReviseEntity revise) {
        this.revise = revise;
        return this;
    }

    public String getProductCode() {
        return productCode;
    }

    public ReviseD5LogsEntity setProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public String getProducerInn() {
        return producerInn;
    }

    public ReviseD5LogsEntity setProducerInn(String producerInn) {
        this.producerInn = producerInn;
        return this;
    }

    public String getProducerKpp() {
        return producerKpp;
    }

    public ReviseD5LogsEntity setProducerKpp(String producerKpp) {
        this.producerKpp = producerKpp;
        return this;
    }

    public Double getReleaseD6Volume() {
        return releaseD6Volume;
    }

    public ReviseD5LogsEntity setReleaseD6Volume(Double releaseD6Volume) {
        this.releaseD6Volume = releaseD6Volume;
        return this;
    }

    public Double getReleaseReturnD6Volume() {
        return releaseReturnD6Volume;
    }

    public ReviseD5LogsEntity setReleaseReturnD6Volume(Double releaseReturnD6Volume) {
        this.releaseReturnD6Volume = releaseReturnD6Volume;
        return this;
    }

    public Double getReleaseVolume() {
        return releaseVolume;
    }

    public ReviseD5LogsEntity setReleaseVolume(Double releaseVolume) {
        this.releaseVolume = releaseVolume;
        return this;
    }

    public Double getReleaseReturnKontragentVolume() {
        return releaseReturnKontragentVolume;
    }

    public ReviseD5LogsEntity setReleaseReturnKontragentVolume(Double releaseReturnKontragentVolume) {
        this.releaseReturnKontragentVolume = releaseReturnKontragentVolume;
        return this;
    }

    public Double getReleaseOtherVolume() {
        return releaseOtherVolume;
    }

    public ReviseD5LogsEntity setReleaseOtherVolume(Double releaseOtherVolume) {
        this.releaseOtherVolume = releaseOtherVolume;
        return this;
    }

    public Double getConsumptionOherVolume() {
        return consumptionOherVolume;
    }

    public ReviseD5LogsEntity setConsumptionOherVolume(Double consumptionOherVolume) {
        this.consumptionOherVolume = consumptionOherVolume;
        return this;
    }

    public Double getConsumptionReturnKontragentVolume() {
        return consumptionReturnKontragentVolume;
    }

    public ReviseD5LogsEntity setConsumptionReturnKontragentVolume(Double consumptionReturnKontragentVolume) {
        this.consumptionReturnKontragentVolume = consumptionReturnKontragentVolume;
        return this;
    }

    public Double getReleaseDelta() {
        return releaseDelta;
    }

    public ReviseD5LogsEntity setReleaseDelta(Double releaseDelta) {
        this.releaseDelta = releaseDelta;
        return this;
    }

    public Double getConsumptionVolume() {
        return consumptionVolume;
    }

    public ReviseD5LogsEntity setConsumptionVolume(Double consumptionVolume) {
        this.consumptionVolume = consumptionVolume;
        return this;
    }

    public Double getConsumptionD7Volume() {
        return consumptionD7Volume;
    }

    public ReviseD5LogsEntity setConsumptionD7Volume(Double consumptionD7Volume) {
        this.consumptionD7Volume = consumptionD7Volume;
        return this;
    }

    public Double getConsumptionReturnD7Volume() {
        return consumptionReturnD7Volume;
    }

    public ReviseD5LogsEntity setConsumptionReturnD7Volume(Double consumptionReturnD7Volume) {
        this.consumptionReturnD7Volume = consumptionReturnD7Volume;
        return this;
    }

    public Double getConsumptionDelta() {
        return consumptionDelta;
    }

    public ReviseD5LogsEntity setConsumptionDelta(Double consumptionDelta) {
        this.consumptionDelta = consumptionDelta;
        return this;
    }
}
