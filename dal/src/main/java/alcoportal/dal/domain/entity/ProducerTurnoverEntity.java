package alcoportal.dal.domain.entity;

import javax.persistence.Cacheable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by vladimir akummail@gmail.com on 11/22/15.
 */
@Cacheable(false)
@Entity
public class ProducerTurnoverEntity implements Serializable {

    // @Id
    // Date id;
    @EmbeddedId
    ProducerTurnoverEntityPK producer;
  ///  String inn;
 //   String kpp;
    Double volume;
    Double returnVolume;
    String productCode;


    public Double getVolume() {
        return volume;
    }

    public ProducerTurnoverEntity setVolume(Double volume) {
        this.volume = volume;
        return this;
    }

    public Double getReturnVolume() {
        return returnVolume;
    }

    public ProducerTurnoverEntity setReturnVolume(Double returnVolume) {
        this.returnVolume = returnVolume;
        return this;
    }

    public String getProductCode() {
        return productCode;
    }

    public ProducerTurnoverEntity setProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public ProducerTurnoverEntityPK getProducer() {
        return producer;
    }

    public ProducerTurnoverEntity setProducer(ProducerTurnoverEntityPK producer) {
        this.producer = producer;
        return this;
    }
}