package alcoportal.dal.domain.entity.revise;

import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.other.TurnoverType;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by vladimir akummail@gmail.com on 11/4/15.
 */
@Entity
@Table(name = "revise_logs")
@SequenceGenerator(sequenceName = "revise_logs_sequence", name = "default_generator", allocationSize = 1)
public class ReviseLogEntity extends BaseEntity {

    @OneToOne(optional = false)
    @JoinColumn(name = "revise_id")
    ReviseEntity revise;

    @Column(name = "product_code")
    String productCode;

    @Column(name = "producer_inn")
    String producerInn;

    @Column(name = "producer_kpp")
    String producerKpp;

    @Column(name = "inn")
    String inn;

    @Column(name = "kpp")
    String kpp;

    @Column(name = "kontragent_inn")
    String kontragentInn;

    @Column(name = "kontragent_kpp")
    String kontragentKpp;

    @Column(name = "number_ttn")
    String numberTTN;

    @Column(name = "number_gtd")
    String numberGTD;

    @Column(name = "date_at")
    Date date;

    @Column(name = "type")
    @Enumerated(EnumType.ORDINAL)
    TurnoverType type;

    @Column(name = "volume")
    double volume;

    @Column(name = "ur2_product_code")
    String ur2ProductCode;

    @Column(name = "ur2_producer_inn")
    String ur2ProducerInn;

    @Column(name = "ur2_producer_kpp")
    String ur2ProducerKpp;

    @Column(name = "ur2_inn")
    String ur2inn;

    @Column(name = "ur2_kpp")
    String ur2kpp;

    @Column(name = "ur2_kontragent_inn")
    String ur2KontragentInn;

    @Column(name = "ur2_kontragent_kpp")
    String ur2KontragentKpp;

    @Column(name = "ur2_number_ttn")
    String ur2NumberTTN;

    @Column(name = "ur2_number_gtd")
    String ur2NumberGTD;

    @Column(name = "ur2_date_at")
    Date ur2Date;

    @Column(name = "ur2_type")
    @Enumerated(EnumType.ORDINAL)
    TurnoverType ur2Type;

    @Column(name = "ur2_volume")
    double ur2Volume;


    @Column(name = "delta_volume")
    double delta;


    public ReviseEntity getRevise() {
        return revise;
    }

    public ReviseLogEntity setRevise(ReviseEntity revise) {
        this.revise = revise;
        return this;
    }

    public String getProductCode() {
        return productCode;
    }

    public ReviseLogEntity setProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public String getProducerInn() {
        return producerInn;
    }

    public ReviseLogEntity setProducerInn(String producerInn) {
        this.producerInn = producerInn;
        return this;
    }

    public String getProducerKpp() {
        return producerKpp;
    }

    public ReviseLogEntity setProducerKpp(String producerKpp) {
        this.producerKpp = producerKpp;
        return this;
    }

    public String getInn() {
        return inn;
    }

    public ReviseLogEntity setInn(String inn) {
        this.inn = inn;
        return this;
    }

    public String getKpp() {
        return kpp;
    }

    public ReviseLogEntity setKpp(String kpp) {
        this.kpp = kpp;
        return this;
    }

    public String getKontragentInn() {
        return kontragentInn;
    }

    public ReviseLogEntity setKontragentInn(String kontragentInn) {
        this.kontragentInn = kontragentInn;
        return this;
    }

    public String getKontragentKpp() {
        return kontragentKpp;
    }

    public ReviseLogEntity setKontragentKpp(String kontragentKpp) {
        this.kontragentKpp = kontragentKpp;
        return this;
    }

    public String getNumberTTN() {
        return numberTTN;
    }

    public ReviseLogEntity setNumberTTN(String numberTTN) {
        this.numberTTN = numberTTN;
        return this;
    }

    public String getNumberGTD() {
        return numberGTD;
    }

    public ReviseLogEntity setNumberGTD(String numberGTD) {
        this.numberGTD = numberGTD;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public ReviseLogEntity setDate(Date date) {
        this.date = date;
        return this;
    }

    public TurnoverType getType() {
        return type;
    }

    public ReviseLogEntity setType(TurnoverType type) {
        this.type = type;
        return this;
    }

    public double getVolume() {
        return volume;
    }

    public ReviseLogEntity setVolume(double volume) {
        this.volume = volume;
        return this;
    }

    public String getUr2ProductCode() {
        return ur2ProductCode;
    }

    public ReviseLogEntity setUr2ProductCode(String ur2ProductCode) {
        this.ur2ProductCode = ur2ProductCode;
        return this;
    }

    public String getUr2ProducerInn() {
        return ur2ProducerInn;
    }

    public ReviseLogEntity setUr2ProducerInn(String ur2ProducerInn) {
        this.ur2ProducerInn = ur2ProducerInn;
        return this;
    }

    public String getUr2ProducerKpp() {
        return ur2ProducerKpp;
    }

    public ReviseLogEntity setUr2ProducerKpp(String ur2ProducerKpp) {
        this.ur2ProducerKpp = ur2ProducerKpp;
        return this;
    }

    public String getUr2inn() {
        return ur2inn;
    }

    public ReviseLogEntity setUr2inn(String ur2inn) {
        this.ur2inn = ur2inn;
        return this;
    }

    public String getUr2kpp() {
        return ur2kpp;
    }

    public ReviseLogEntity setUr2kpp(String ur2kpp) {
        this.ur2kpp = ur2kpp;
        return this;
    }

    public String getUr2KontragentInn() {
        return ur2KontragentInn;
    }

    public ReviseLogEntity setUr2KontragentInn(String ur2KontragentInn) {
        this.ur2KontragentInn = ur2KontragentInn;
        return this;
    }

    public String getUr2KontragentKpp() {
        return ur2KontragentKpp;
    }

    public ReviseLogEntity setUr2KontragentKpp(String ur2KontragentKpp) {
        this.ur2KontragentKpp = ur2KontragentKpp;
        return this;
    }

    public String getUr2NumberTTN() {
        return ur2NumberTTN;
    }

    public ReviseLogEntity setUr2NumberTTN(String ur2NumberTTN) {
        this.ur2NumberTTN = ur2NumberTTN;
        return this;
    }

    public String getUr2NumberGTD() {
        return ur2NumberGTD;
    }

    public ReviseLogEntity setUr2NumberGTD(String ur2NumberGTD) {
        this.ur2NumberGTD = ur2NumberGTD;
        return this;
    }

    public Date getUr2Date() {
        return ur2Date;
    }

    public ReviseLogEntity setUr2Date(Date ur2Date) {
        this.ur2Date = ur2Date;
        return this;
    }

    public TurnoverType getUr2Type() {
        return ur2Type;
    }

    public ReviseLogEntity setUr2Type(TurnoverType ur2Type) {
        this.ur2Type = ur2Type;
        return this;
    }

    public double getUr2Volume() {
        return ur2Volume;
    }

    public ReviseLogEntity setUr2Volume(double ur2Volume) {
        this.ur2Volume = ur2Volume;
        return this;
    }

    public double getDelta() {
        return delta;
    }

    public ReviseLogEntity setDelta(double delta) {
        this.delta = delta;
        return this;
    }
}
