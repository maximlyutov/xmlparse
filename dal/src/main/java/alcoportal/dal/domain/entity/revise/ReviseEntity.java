package alcoportal.dal.domain.entity.revise;

import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 11/3/15.
 */
@Entity
@Table(name = "revises")
@SequenceGenerator(sequenceName = "revises_sequence", name = "default_generator", allocationSize = 1)
public class ReviseEntity extends BaseEntity {

    @Column(name = "create_at")
    private Date createAt;

    @OneToOne(optional = false)
    @JoinColumn(name = "document_history_id")
    DocumentHistoryEntity documentHistory;

    @Column(name = "revise_state")
    @Enumerated(EnumType.ORDINAL)
    private ReviseState reviseState;

    private String description;

    @Transient
    private List<ReviseLogEntity> reviseLogs;

    // @OneToMany(mappedBy = "revise", cascade = CascadeType.ALL)
    @Transient
    private List<ReviseD5LogsEntity> reviseD5Logs;

    public String getDescription() {
        return description;
    }

    public ReviseEntity setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<ReviseD5LogsEntity> getReviseD5Logs() {
        return reviseD5Logs;
    }

    public ReviseEntity setReviseD5Logs(List<ReviseD5LogsEntity> reviseD5Logs) {
        this.reviseD5Logs = reviseD5Logs;
        return this;
    }

    public List<ReviseLogEntity> getReviseLogs() {

        return reviseLogs;
    }

    public ReviseEntity setReviseLogs(List<ReviseLogEntity> reviseLogs) {
        this.reviseLogs = reviseLogs;
        return this;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public ReviseEntity setCreateAt(Date createAt) {
        this.createAt = createAt;
        return this;
    }

    public DocumentHistoryEntity getDocumentHistory() {
        return documentHistory;
    }

    public ReviseEntity setDocumentHistory(DocumentHistoryEntity documentHistory) {
        this.documentHistory = documentHistory;
        return this;
    }

    public ReviseState getReviseState() {
        return reviseState;
    }

    public ReviseEntity setReviseState(ReviseState reviseState) {
        this.reviseState = reviseState;
        return this;
    }
}
