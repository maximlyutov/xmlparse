package alcoportal.dal.domain.entity.userInfo;


import alcoportal.dal.domain.entity.BaseEntity;
import alcoportal.dal.domain.entity.UserEntity;

import javax.persistence.*;

/**
 * Created by good on 29.10.2015.
 */
@Entity
@Table(name = "subdivision")
@SequenceGenerator(sequenceName = "subdivision_sequence", name = "default_generator", allocationSize = 1)
public class SubdivisionEntity extends BaseEntity {
    private Long kpp;
    @Column(name = "subject_code")
    private String subjectCode;
    private String address;

    @OneToOne(targetEntity = UserEntity.class)
    private UserEntity user;

    public Long getKpp() {
        return kpp;
    }

    public void setKpp(Long kpp) {
        this.kpp = kpp;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
