package alcoportal.dal.domain.entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by vladimir akummail@gmail.com on 10/5/15.
 */
@Entity
@Table(name = "documents_history")
@SequenceGenerator(sequenceName = "documents_history_sequence", name = "default_generator", allocationSize = 1)
public class DocumentHistoryEntity extends BaseEntity {

    @Column(name = "file_name")
    String fileName;

    @Basic(fetch = FetchType.LAZY)
    @Type(type = "org.hibernate.type.BinaryType")
    private byte[] document;

    @Column(name = "already_revised")
    private boolean alreadyRevised;

    @Column(name = "validate_state")
    private boolean validateState;

    private String formName;

    @Column(name = "create_at")
    private Date createAt;

    private String description;

    private String year;

    private String quarter;

    @OneToOne(optional = false)
    private UserEntity user;

    public boolean getAlreadyRevised() {
        return alreadyRevised;
    }

    public void setAlreadyRevised(boolean alreadyRevised) {
        this.alreadyRevised = alreadyRevised;
    }

    @Column(name = "full_revise_state")
    private boolean fullReviseState;

    public boolean isFullReviseState() {
        return fullReviseState;
    }

    public DocumentHistoryEntity setFullReviseState(boolean fullReviseState) {
        this.fullReviseState = fullReviseState;
        return this;
    }

    public UserEntity getUser() {
        return user;
    }

    public DocumentHistoryEntity setUser(UserEntity user) {
        this.user = user;
        return this;
    }

    public String getYear() {
        return year;
    }

    public DocumentHistoryEntity setYear(String year) {
        this.year = year;
        return this;
    }

    public String getQuarter() {
        return quarter;
    }

    public DocumentHistoryEntity setQuarter(String quarter) {
        this.quarter = quarter;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public DocumentHistoryEntity setDescription(String description) {
        this.description = description;
        return this;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public DocumentHistoryEntity setCreateAt(Date createAt) {
        this.createAt = createAt;
        return this;
    }

    ////TODO Переделать на enum
    public String getFormName() {
        return formName;
    }

    public DocumentHistoryEntity setFormName(String formName) {
        this.formName = formName;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public DocumentHistoryEntity setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public byte[] getDocument() {
        return document;
    }

    public DocumentHistoryEntity setDocument(byte[] document) {
        this.document = document;
        return this;
    }

    public boolean isValidateState() {
        return validateState;
    }

    public DocumentHistoryEntity setValidateState(boolean validateState) {
        this.validateState = validateState;
        return this;
    }
}
