package alcoportal.dal.domain.other;

/**
 * Created by vladimir akummail@gmail.com on 10/21/15.
 */
public enum TurnoverType {
    SUPPLY,
    RETURN;

    @Override
    public String toString() {
        switch (this) {
            case RETURN:
                return "Возврат";
            case SUPPLY:
                return "Поставка";
            default:
                return "";
        }
    }
}
