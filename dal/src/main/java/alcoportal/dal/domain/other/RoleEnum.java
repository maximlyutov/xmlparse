package alcoportal.dal.domain.other;

/**
 * Created by good on 03.10.2015.
 */
public enum RoleEnum {
    ANONYMOUS,
    ROLE_USER,
    ROLE_ADMIN
}
