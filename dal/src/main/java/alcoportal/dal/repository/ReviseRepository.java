package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.revise.ReviseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 11/3/15.
 */
public interface ReviseRepository extends JpaRepository<ReviseEntity, Long> {

    @Query(value = "select r.* from revises r, documents_history dh where dh.user_id=:userId and dh.full_revise_state=true and r.document_history_id=dh.id  order by id desc", nativeQuery = true)
    List<ReviseEntity> getRevises(@Param("userId") long userId);

    ReviseEntity findOneByDocumentHistory(DocumentHistoryEntity documentHistory);
}
