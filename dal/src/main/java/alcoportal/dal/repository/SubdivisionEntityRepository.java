package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.userInfo.OrganisationEntity;
import alcoportal.dal.domain.entity.userInfo.SubdivisionEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by good on 03.10.2015.
 */

public interface SubdivisionEntityRepository extends CrudRepository<SubdivisionEntity, Long> {

    public List<SubdivisionEntity> findByUser(UserEntity user);

}
