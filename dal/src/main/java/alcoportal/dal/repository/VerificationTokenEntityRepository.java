package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.VerificationTokenEntity;
import org.springframework.data.repository.CrudRepository;

public interface VerificationTokenEntityRepository extends CrudRepository<VerificationTokenEntity, Long> {

    VerificationTokenEntity findByToken(String token);

    VerificationTokenEntity findByUser(UserEntity user);

}
