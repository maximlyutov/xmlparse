package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.revise.ReviseEntity;
import alcoportal.dal.domain.entity.revise.ReviseLogEntity;
import alcoportal.dal.domain.other.TurnoverType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 12/1/15.
 */
public interface ReviseLogCustomRepository {
    ReviseLogEntity findOneBy(
            ReviseEntity revise,
            String productCode,
            String producerInn,
            String producerKpp,
            String numberTTN,
            String numberGTD,
            Date date,
            TurnoverType type,
            String kontragentInn,
            String kontragentKpp
    );
}
