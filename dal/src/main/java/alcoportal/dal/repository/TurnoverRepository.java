package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.D6.KontragentEntity;
import alcoportal.dal.domain.entity.D6.ProducerEntity;
import alcoportal.dal.domain.entity.D6.TurnoverEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 10/22/15.
 */

public interface TurnoverRepository extends CrudRepository<TurnoverEntity, Long> {

    List<TurnoverEntity> findAllByKontragentIn(List<KontragentEntity> kontragent);

    List<TurnoverEntity> findAllByDocumentHistory(DocumentHistoryEntity documentHistory);

    List<TurnoverEntity> findAllByProducer(ProducerEntity producer);

    @Query(value = "SELECT DISTINCT (KPP) from turnovers WHERE document_history_id = :documentHistoryId", nativeQuery = true)
    List<String> getAllKpp(@Param("documentHistoryId") long documentHistoryId);

}
