package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.D6.ProducerEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 10/22/15.
 */

public interface ProducerRepository extends CrudRepository<ProducerEntity, Long> {

    List<ProducerEntity> findAllByDocumentHistory(DocumentHistoryEntity documentHistory);
}