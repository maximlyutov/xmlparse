package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.revise.ReviseEntity;
import alcoportal.dal.domain.entity.revise.ReviseLogEntity;
import alcoportal.dal.domain.other.TurnoverType;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;

/**
 * Created by vladimir akummail@gmail.com on 12/1/15.
 */
@Repository
public class ReviseLogCustomRepositoryImpl implements ReviseLogCustomRepository {


    public static final String getReviseLog =
            "SELECT *\n" +
                    "FROM revise_logs\n" +
                    "WHERE\n" +
                    "  revise_id=:revise_id AND\n" +
                    "  product_code = :product_code AND\n" +
                    "  producer_inn = :producer_inn AND\n" +
                    "  producer_kpp = :producer_kpp AND\n" +
                    "  number_gtd = :number_gtd AND\n" +
                    "  number_ttn = :number_ttn AND\n" +
                    "  date_at = :date_at AND\n" +
                    "  kontragent_inn = :kontragent_inn AND\n" +
                    "  kontragent_kpp = :kontragent_kpp AND\n" +
                    "  type = :type\n";


    @PersistenceContext
    private EntityManager em;

    @Override
    public ReviseLogEntity findOneBy(
            ReviseEntity revise,
            String productCode,
            String producerInn,
            String producerKpp,
            String numberTTN,
            String numberGTD,
            Date date,
            TurnoverType type,
            String kontragentInn,
            String kontragentKpp) {

        Query nativeQuery = em.createNativeQuery(getReviseLog, ReviseLogEntity.class);

        nativeQuery.setParameter("revise_id", revise.getId());
        nativeQuery.setParameter("product_code", productCode);
        nativeQuery.setParameter("producer_inn", producerInn);
        nativeQuery.setParameter("producer_kpp", producerKpp);
        nativeQuery.setParameter("number_gtd", numberGTD);
        nativeQuery.setParameter("number_ttn", numberTTN);
        nativeQuery.setParameter("date_at", date);
        nativeQuery.setParameter("kontragent_inn", kontragentInn);
        nativeQuery.setParameter("kontragent_kpp", kontragentKpp);
        nativeQuery.setParameter("type", type.ordinal());
        ReviseLogEntity reviseLogEntity = null;
        try {
            reviseLogEntity = (ReviseLogEntity) nativeQuery.getSingleResult();
        } finally {
            return reviseLogEntity;
        }

    }
}
