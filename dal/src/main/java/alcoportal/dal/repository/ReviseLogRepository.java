package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.revise.ReviseEntity;
import alcoportal.dal.domain.entity.revise.ReviseLogEntity;
import alcoportal.dal.domain.other.TurnoverType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by ----------------------- on 11/4/15.
 */
public interface ReviseLogRepository extends CrudRepository<ReviseLogEntity, Long> {

    List<ReviseLogEntity> findAllByRevise(ReviseEntity revise);

    @Query(value = "SELECT * FROM revise_logs where revise_id=:reviseId order by GREATEST(product_code,ur2_product_code,producer_inn,ur2_producer_inn,producer_kpp,ur2_producer_kpp) desc", nativeQuery = true)
    List<ReviseLogEntity> finAllByReviseId(@Param("reviseId") long reviseId);
}