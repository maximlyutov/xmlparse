package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by good on 03.10.2015.
 */

public interface UserEntityRepository extends CrudRepository<UserEntity, Long> {

    public UserEntity findByEmail(String email);

    public UserEntity findByEmailIgnoreCase(String email);

    public UserEntity findByInn(Long inn);
}
