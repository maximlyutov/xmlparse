package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.userInfo.OrganisationEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by good on 03.10.2015.
 */

public interface OrganisationEntityRepository extends CrudRepository<OrganisationEntity, Long> {

    public OrganisationEntity findByUser(UserEntity user);

}
