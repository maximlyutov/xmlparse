package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.ValidationErrorEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by vladimir akummail@gmail.com on 10/18/15.
 */

public interface ValidationErrorRepository extends CrudRepository<ValidationErrorEntity, Long> {
    ValidationErrorEntity findByFormNameAndElementName(String formName, String elementName);
}
