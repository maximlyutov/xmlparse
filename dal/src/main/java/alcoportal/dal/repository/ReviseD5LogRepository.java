package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.revise.ReviseD5LogsEntity;
import alcoportal.dal.domain.entity.revise.ReviseEntity;
import alcoportal.dal.domain.entity.revise.ReviseLogEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 11/21/15.
 */

public interface ReviseD5LogRepository extends CrudRepository<ReviseD5LogsEntity, Long> {

    @Query(value = "SELECT count(*) FROM revise_d5_logs where revise_id=:reviseId", nativeQuery = true)
    int getCountForReviseId(@Param("reviseId") long reviseId);

    ReviseD5LogsEntity findOneByReviseAndProductCodeAndProducerInnAndProducerKpp(ReviseEntity revise, String productCode, String producerInn, String producerKpp);

    @Query(value = "SELECT * FROM revise_d5_logs where revise_id=:reviseId order by product_code,producer_inn,producer_kpp desc", nativeQuery = true)
    List<ReviseD5LogsEntity> finAllByReviseId(@Param("reviseId") long reviseId);

}
