package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.UserRoleEntity;
import alcoportal.dal.domain.other.RoleEnum;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by good on 07.10.2015.
 */
public interface UserRoleEntityRepository extends CrudRepository<UserRoleEntity, Long> {
    public UserRoleEntity findByTitle(RoleEnum title);
}
