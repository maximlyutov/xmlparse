package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.D6.KontragentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 10/22/15.
 */

public interface KontragentRepository extends JpaRepository<KontragentEntity, Long> {

    @Query(value = "select k.* from kontragents k, kontragents k1 where k1.document_history_id = :baseDocumentId and k.inn = k1.inn and k.document_history_id = :scanDocumentId", nativeQuery = true)
    List<KontragentEntity> getKontragents(@Param("baseDocumentId") long baseDocumentId, @Param("scanDocumentId") long scanDocumentId);


    @Query(value = "select * from kontragents where document_history_id = :scanDocumentId and inn =:inn", nativeQuery = true)
    List<KontragentEntity> getKontragentsNew(@Param("scanDocumentId") long scanDocumentId, @Param("inn") String inn);

    @Query(value = "select * from kontragents where document_history_id = :scanDocumentId and inn =:inn and kpp in :kpp", nativeQuery = true)
    List<KontragentEntity> getKontragentsNew(@Param("scanDocumentId") long scanDocumentId, @Param("inn") String inn, @Param("kpp") List<String> kpp);
}
