package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.TurnoverD5;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 11/21/15.
 */
public interface TurnoverD5Repository extends CrudRepository<TurnoverD5, Long> {
    TurnoverD5 findOneByDocumentHistoryAndInnAndKppAndProductCode(DocumentHistoryEntity documentHistory, String inn, String kpp, String productCode);


    List<TurnoverD5> findAllByDocumentHistory(DocumentHistoryEntity documentHistory);

}
