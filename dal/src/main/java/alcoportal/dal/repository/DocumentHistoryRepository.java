package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 10/7/15.
 */

public interface DocumentHistoryRepository extends CrudRepository<DocumentHistoryEntity, Long> {

    List<DocumentHistoryEntity> findAllByUserOrderByCreateAtDesc(UserEntity user);

    List<DocumentHistoryEntity> findAllByIdNotAndFormNameAndYearAndQuarterAndFullReviseStateTrueAndUserNot(Long id, String formName, String year, String quarter, UserEntity user);

    DocumentHistoryEntity findOneByUserAndFormNameAndYearAndQuarterAndFullReviseStateTrue(UserEntity user, String formName, String year, String quarter);

    DocumentHistoryEntity findOneByUserAndId(UserEntity userEntity, long documentHistoryId);
}
