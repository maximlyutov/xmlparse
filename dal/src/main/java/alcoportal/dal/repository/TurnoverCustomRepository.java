package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.ProducerTurnoverEntity;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 11/22/15.
 */
public interface TurnoverCustomRepository {
    List<ProducerTurnoverEntity> getTurnoversForReviseD5(long documenHistoriId);
}
