package alcoportal.dal.repository;

import alcoportal.dal.domain.entity.PasswordResetTokenEntity;
import alcoportal.dal.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by good on 21.11.2015.
 */
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetTokenEntity, Long> {

    PasswordResetTokenEntity findByToken(String token);

    PasswordResetTokenEntity findByUser(UserEntity user);

}
