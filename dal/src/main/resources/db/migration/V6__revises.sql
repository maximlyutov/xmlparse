CREATE SEQUENCE revises_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE revises
(
  id                  BIGINT DEFAULT nextval('revises_sequence' :: REGCLASS)            NOT NULL PRIMARY KEY,
  version             BIGINT,
  create_at           TIMESTAMP WITHOUT TIME ZONE,
  document_history_id BIGINT REFERENCES documents_history (id) ON DELETE CASCADE UNIQUE NOT NULL,
  revise_state        CHARACTER                                                         NOT NULL,
  description         CHARACTER VARYING                                                 NULL
);