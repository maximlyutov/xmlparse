CREATE SEQUENCE documents_history_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE documents_history
(
  id                BIGINT DEFAULT nextval('documents_history_sequence' :: REGCLASS)          NOT NULL PRIMARY KEY,
  version           BIGINT,
  file_name         CHARACTER VARYING(250)                                                    NOT NULL,
  form_name         CHARACTER VARYING(250)                                                    NULL,
  validate_state    BOOLEAN                                                                   NOT NULL DEFAULT FALSE,
  document          BYTEA                                                                     NOT NULL,
  create_at         TIMESTAMP WITHOUT TIME ZONE,
  description       CHARACTER VARYING                                                         NULL,
  year              CHARACTER VARYING(250),
  quarter           CHARACTER VARYING(250),
  user_id           BIGINT REFERENCES users (id) ON DELETE CASCADE                            NOT NULL,
  full_revise_state BOOLEAN                                                                   NOT NULL DEFAULT FALSE,
  already_revised BOOLEAN                                                                   NOT NULL DEFAULT FALSE
);