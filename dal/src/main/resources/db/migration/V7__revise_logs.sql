CREATE SEQUENCE revise_logs_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE revise_logs
(
  id                 BIGINT DEFAULT nextval('revise_logs_sequence' :: REGCLASS)     NOT NULL PRIMARY KEY,
  version            BIGINT,
  revise_id          BIGINT REFERENCES revises (id) ON DELETE CASCADE               NOT NULL,

  producer_inn       CHARACTER VARYING(250),
  producer_kpp       CHARACTER VARYING(250),
  inn                CHARACTER VARYING(250),
  kpp                CHARACTER VARYING(250),
  kontragent_inn     CHARACTER VARYING(250),
  kontragent_kpp     CHARACTER VARYING(250),
  product_code       CHARACTER VARYING(250),
  number_ttn         CHARACTER VARYING(250),
  number_gtd         CHARACTER VARYING(250),
  type               INT,
  date_at            DATE,
  volume             NUMERIC(20, 4),

  ur2_product_code   CHARACTER VARYING(250),
  ur2_producer_inn   CHARACTER VARYING(250),
  ur2_producer_kpp   CHARACTER VARYING(250),
  ur2_inn            CHARACTER VARYING(250),
  ur2_kpp            CHARACTER VARYING(250),
  ur2_kontragent_inn CHARACTER VARYING(250),
  ur2_kontragent_kpp CHARACTER VARYING(250),
  ur2_number_ttn     CHARACTER VARYING(250),
  ur2_number_gtd     CHARACTER VARYING(250),
  ur2_type           INT,
  ur2_date_at        DATE,
  ur2_volume         NUMERIC(20, 4),

  delta_volume       NUMERIC(20, 4)
)

