CREATE SEQUENCE users_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE users
(
  id bigint DEFAULT nextval('users_sequence'::regclass) NOT NULL,
    email character varying(250),
    password character varying(250) NOT NULL,
    username character varying(250) NOT NULL,
    phone_number character varying(250),
    fio character varying(250),
    inn bigint not null,
    version bigint
);

CREATE SEQUENCE roles_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE roles
(
  id bigint NOT NULL DEFAULT nextval('roles_sequence'::regclass),
  title character varying(100) NOT NULL,
  version bigint,
  CONSTRAINT roles_pkey PRIMARY KEY (id),
  CONSTRAINT roles_title_key UNIQUE (title)
);

CREATE TABLE users_roles (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT users_roles_pkey PRIMARY KEY (user_id, role_id);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);

insert into roles (title) VALUES('ROLE_USER');
insert into roles (title) VALUES('ROLE_ADMIN');

CREATE SEQUENCE verification_token_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE verification_token
(
  id bigint DEFAULT nextval('verification_token_sequence'::regclass) NOT NULL,
    token character varying(250),
    expiry_date DATE,
    user_id bigint NOT NULL,
    version bigint
);