CREATE SEQUENCE kontragents_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE kontragents
(
  id                      BIGINT DEFAULT nextval(
      'kontragents_sequence' :: REGCLASS)                                                            NOT NULL PRIMARY KEY,
  inn                     CHARACTER VARYING(250)                                                     NOT NULL,
  kpp                     CHARACTER VARYING(250)                                                     NOT NULL,
  license_number          CHARACTER VARYING(250)                                                     NOT NULL,
  start_date_licensing    TIMESTAMP WITHOUT TIME ZONE,
  expiration_date_license TIMESTAMP WITHOUT TIME ZONE,
  document_history_id     BIGINT REFERENCES documents_history (id) ON DELETE CASCADE                 NOT NULL,
  version                 BIGINT
);


CREATE SEQUENCE producers_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE producers
(
  id                  BIGINT DEFAULT nextval('producers_sequence' :: REGCLASS)                    NOT NULL PRIMARY KEY,
  inn                 CHARACTER VARYING(250)                                                      NOT NULL,
  kpp                 CHARACTER VARYING(250)                                                      NOT NULL,
  document_history_id BIGINT REFERENCES documents_history (id) ON DELETE CASCADE                  NOT NULL,
  version             BIGINT
);


CREATE SEQUENCE turnovers_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE turnovers
(
  id                  BIGINT DEFAULT nextval('turnovers_sequence' :: REGCLASS)                    NOT NULL PRIMARY KEY,
  product_code        CHARACTER VARYING(250)                                                      NOT NULL,
  purchase_date       TIMESTAMP WITHOUT TIME ZONE,
  number_ttn          CHARACTER VARYING(250)                                                      NOT NULL,
  number_gtd          CHARACTER VARYING(250)                                                      NOT NULL,
  type                CHARACTER VARYING(250)                                                      NOT NULL,
  volume              NUMERIC(20, 4)                                                              NOT NULL,
  document_history_id BIGINT REFERENCES documents_history (id) ON DELETE CASCADE                  NOT NULL,
  producer_id         BIGINT REFERENCES producers (id) ON DELETE CASCADE                          NOT NULL,
  kontragent_id       BIGINT REFERENCES kontragents (id) ON DELETE CASCADE                        NOT NULL,
  version             BIGINT,
  kpp                 CHARACTER VARYING(250)                                                      NULL
);