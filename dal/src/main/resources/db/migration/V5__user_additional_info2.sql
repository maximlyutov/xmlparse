CREATE SEQUENCE organisation_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE organisation
(
  id                      BIGINT DEFAULT nextval('organisation_sequence' :: REGCLASS) NOT NULL PRIMARY KEY,
  inn                     BIGINT,
  kpp                     BIGINT,
  name                    CHARACTER VARYING(512),
  fio_dir                 CHARACTER VARYING(512),
  fio_buh                 CHARACTER VARYING(512),
  subject_code            CHARACTER VARYING(1024),
  ur_address              CHARACTER VARYING(1024),
  email                   CHARACTER VARYING(256),
  org_type                character varying(256),
  version                 BIGINT,
  user_id                 BIGINT not null
);

CREATE SEQUENCE subdivision_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE subdivision
(
  id                      BIGINT DEFAULT nextval('subdivision_sequence' :: REGCLASS) NOT NULL PRIMARY KEY,
  kpp                     BIGINT,
  subject_code            CHARACTER VARYING(512),
  address                 CHARACTER VARYING(512),
  user_id                 BIGINT not null,
   version                 BIGINT
);


CREATE SEQUENCE license_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE license
(
  id                      BIGINT DEFAULT nextval('license_sequence' :: REGCLASS) NOT NULL PRIMARY KEY,
  user_id                 BIGINT not null,
  subdivision_id          BIGINT not null,
  work_type                 CHARACTER VARYING(512),
   date_start                 CHARACTER VARYING(512),
    date_end                 CHARACTER VARYING(512),
     number                 CHARACTER VARYING(512),
      number_blank                 CHARACTER VARYING(512),
  version                 BIGINT
);

CREATE SEQUENCE remember_me_token_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE remember_me_token
(
  id                      BIGINT DEFAULT nextval('remember_me_token_sequence' :: REGCLASS) NOT NULL PRIMARY KEY,
  username                 CHARACTER VARYING(512),
  series                 CHARACTER VARYING(512),
   token                 CHARACTER VARYING(512),
    date                 DATE,
  version                 BIGINT
);