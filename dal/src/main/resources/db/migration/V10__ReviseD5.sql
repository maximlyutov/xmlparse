CREATE SEQUENCE revise_d5_logs_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE revise_d5_logs
(
  id                                   BIGINT DEFAULT nextval('revise_d5_logs_sequence' ::
                                                              REGCLASS)                                                         NOT NULL PRIMARY KEY,
  version                              BIGINT,
  revise_id                            BIGINT REFERENCES revises (id) ON DELETE CASCADE                                         NOT NULL,

  producer_inn                         CHARACTER VARYING(250)                                                                   NOT NULL,
  producer_kpp                         CHARACTER VARYING(250)                                                                   NOT NULL,
  product_code                         CHARACTER VARYING(250)                                                                   NOT NULL,

  release_d6_volume                    NUMERIC(20, 4),
  release_return_d6_volume             NUMERIC(20, 4),
  release_volume                       NUMERIC(20, 4),
  release_other_volume                 NUMERIC(20, 4),
  release_return_kontragent_volume     NUMERIC(20, 4),

  release_delta                        NUMERIC(20, 4),

  consumption_volume                   NUMERIC(20, 4),
  consumption_other_volume             NUMERIC(20, 4),
  consumption_return_kontragent_volume NUMERIC(20, 4),
  consumption_d7_volume                NUMERIC(20, 4),
  consumption_return_d7_volume         NUMERIC(20, 4),
  consumption_delta                    NUMERIC(20, 4)
)