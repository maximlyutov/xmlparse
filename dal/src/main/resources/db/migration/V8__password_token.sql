CREATE SEQUENCE password_token_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE password_token
(
  id bigint DEFAULT nextval('password_token_sequence'::regclass) NOT NULL,
    token character varying(250),
    expiry_date DATE,
    user_id bigint NOT NULL,
    version bigint
);