CREATE SEQUENCE turnovers_d5_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;


CREATE TABLE turnovers_d5
(
  id                                   BIGINT DEFAULT nextval(
      'turnovers_d5_sequence' ::
      REGCLASS)                                                                                                                    NOT NULL PRIMARY KEY,
  document_history_id                  BIGINT REFERENCES documents_history (id) ON DELETE CASCADE                                  NOT NULL,
  inn                                  CHARACTER VARYING(250)                                                                      NOT NULL,
  kpp                                  CHARACTER VARYING(250)                                                                      NOT NULL,
  product_code                         CHARACTER VARYING(250)                                                                      NOT NULL,
  release_volume                       NUMERIC(20, 4),
  release_other_volume                 NUMERIC(20, 4),
  release_return_kontragent_volume     NUMERIC(20, 4),
  consumption_volume                   NUMERIC(20, 4),
  consumption_other_volume             NUMERIC(20, 4),
  consumption_return_kontragent_volume NUMERIC(20, 4),
  version                              BIGINT
)
