CREATE SEQUENCE validation_errors_sequence
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

CREATE TABLE validation_errors
(
  id             BIGINT DEFAULT nextval('validation_errors_sequence' :: REGCLASS) NOT NULL PRIMARY KEY,
  form_name      CHARACTER VARYING(250)                                           NOT NULL,
  element_name   CHARACTER VARYING(250)                                           NOT NULL,
  description    CHARACTER VARYING(250)                                           NOT NULL,
  version        BIGINT
  );