package alcoportal.utils;

import alcoportal.dto.FileNameDTO;
import alcoportal.exseptions.ValidateExseption;
import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
public class FormNameUtils {
    /**
     * Создает FileNameDTO, из данных в имени файла по такому шаблону  (R_О_Z_ddmmgggg_N.xml).
     *
     * @param name пример D7_7718929024_09_19102015_5242A31F-0FFF-4FAB-981C-D4E88E1ED35E.xml
     */
    public static FileNameDTO GetNameDTOFromString(String name) throws ValidateExseption, UnsupportedEncodingException {

        Pattern formType = Pattern.compile("(^D[5,6,7])_");
        Pattern inn = Pattern.compile("D[5,6,7]\\D(\\d+)_");
        Pattern quartel = Pattern.compile("D[5,6,7]\\D\\d+\\D(0[0,3,6,9])_");
        Pattern datePatern = Pattern.compile("D[5,6,7]\\D\\d+\\D0[0,3,6,9]\\D(\\d{8})_");

        FileNameDTO fileNameDTO = new FileNameDTO();

        try {
            Matcher formTypeMatcher = formType.matcher(name);
            if (formTypeMatcher.find()) {
                String formName = formTypeMatcher.group(1);
                fileNameDTO.setFormName(formName);
            } else {
                throw new Exception("тип формы");
            }

            Matcher innMatcher = inn.matcher(name);
            if (innMatcher.find()) {
                fileNameDTO.setIin(innMatcher.group(1));
            } else {
                throw new Exception("инн");
            }

            Matcher quartelMatcher = quartel.matcher(name);
            if (quartelMatcher.find()) {
                fileNameDTO.setQuartal(quartelMatcher.group(1));
            } else {
                throw new Exception("квартал");
            }

            try {
                Matcher dateMatcher = datePatern.matcher(name);
                if (dateMatcher.find()) {
                    fileNameDTO.setDate(new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH).parse(dateMatcher.group(1)));
                } else {
                    throw new Exception("дата");
                }

            } catch (Exception e) {
                throw new Exception("дата");
            }

            return fileNameDTO;
        } catch (Exception e) {
            throw new ValidateExseption("Неправильное имя файла параметр '" + e.getMessage() + "'  " + name);
        }

    }
}
