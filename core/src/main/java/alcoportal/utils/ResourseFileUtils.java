package alcoportal.utils;

import javax.xml.transform.stream.StreamSource;
import java.io.FileNotFoundException;

/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
public class ResourseFileUtils {
    public static StreamSource getFileFromResource(String fileName, ClassLoader classLoader) throws NoSuchFieldException, FileNotFoundException {
        return new StreamSource(classLoader.getResourceAsStream(fileName));
    }
}
