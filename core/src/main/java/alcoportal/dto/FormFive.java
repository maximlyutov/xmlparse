package alcoportal.dto;


import javax.xml.bind.annotation.XmlElement;
/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
public class FormFive {

    @XmlElement(name = "КодСтраны")
    private String countryCode;
    @XmlElement(name = "Индекс")
    private String Index;
    @XmlElement(name = "КодРегион")
    private String regionCode;
    @XmlElement(name = "Район")
    private String district;
    @XmlElement(name = "Город")
    private String city;
    @XmlElement(name = "НаселПункт")
    private String naselPunkt;
    @XmlElement(name = "Улица")
    private String street;
    @XmlElement(name = "Дом")
    private String numberHous;
    @XmlElement(name = "Корпус")
    private String corpus;
    @XmlElement(name = "Кварт")
    private String numberFlat;
}
