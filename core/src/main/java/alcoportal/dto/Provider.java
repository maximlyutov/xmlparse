package alcoportal.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
@XmlRootElement(name = "СведПроизвИмпорт")
public class Provider {
    @XmlAttribute(name = "П000000000004")
    String Name;

    @XmlAttribute(name = "ПN")
    int Number;

    public String getName() {
        return Name;
    }

    public Provider setName(String name) {
        Name = name;
        return this;
    }

    public int getNumber() {
        return Number;
    }

    public Provider setNumber(int number) {
        Number = number;
        return this;
    }
}
