package alcoportal.dto;

import alcoportal.dal.domain.entity.revise.ReviseState;
import alcoportal.utils.DateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vladimir akummail@gmail.com on 10/14/15.
 */
public class DocumentHistoryDTO implements Serializable {

    long id;
    String fileName;

    @JsonProperty("status")
    private boolean validateState;

    @JsonProperty("form")
    private String type;

    @JsonProperty("date")
    @JsonSerialize(using = DateSerializer.class)
    private Date createAt;

    @JsonProperty("analyze")
    private String description;

    private String year;

    private String quarter;

    private ReviseState reviseState;

    private boolean fullReviseState;

    private boolean alreadyRevised;

    public boolean getAlreadyRevised() {
        return alreadyRevised;
    }

    public DocumentHistoryDTO setAlreadyRevised(boolean alreadyRevised) {
        this.alreadyRevised = alreadyRevised;
        return this;
    }

    public boolean isFullReviseState() {
        return fullReviseState;
    }

    public DocumentHistoryDTO setFullReviseState(boolean fullReviseState) {
        this.fullReviseState = fullReviseState;
        return this;
    }

    public ReviseState getReviseState() {
        return reviseState;
    }

    public DocumentHistoryDTO setReviseState(ReviseState reviseState) {
        this.reviseState = reviseState;
        return this;
    }

    public long getId() {
        return id;
    }

    public DocumentHistoryDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getYear() {
        return year;
    }

    public DocumentHistoryDTO setYear(String year) {
        this.year = year;
        return this;
    }

    public String getQuarter() {
        return quarter;
    }

    public DocumentHistoryDTO setQuarter(String quarter) {
        this.quarter = quarter;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public DocumentHistoryDTO setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public boolean isValidateState() {
        return validateState;
    }

    public DocumentHistoryDTO setValidateState(boolean validateState) {
        this.validateState = validateState;
        return this;
    }

    public String getType() {
        return type;
    }

    public DocumentHistoryDTO setType(String type) {
        this.type = type;
        return this;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public DocumentHistoryDTO setCreateAt(Date createAt) {
        this.createAt = createAt;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public DocumentHistoryDTO setDescription(String description) {
        this.description = description;
        return this;
    }
}
