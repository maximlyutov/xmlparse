package alcoportal.dto;

import java.util.Date;
/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
public class FileNameDTO {
    private String formName;
    private String iin;
    private String quartal;
    private Date date;
    private String id;

    public String getFormName() {
        return formName;
    }

    public FileNameDTO setFormName(String formName) {
        this.formName = formName;
        return this;
    }

    public String getIin() {
        return iin;
    }

    public FileNameDTO setIin(String iin) {
        this.iin = iin;
        return this;
    }

    public String getQuartal() {
        return quartal;
    }

    public FileNameDTO setQuartal(String quartal) {
        this.quartal = quartal;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public FileNameDTO setDate(Date date) {
        this.date = date;
        return this;
    }

    public String getId() {
        return id;
    }

    public FileNameDTO setId(String id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileNameDTO fileNameDTO = (FileNameDTO) o;

        if (formName != null ? !formName.equals(fileNameDTO.formName) : fileNameDTO.formName != null) return false;
        if (iin != null ? !iin.equals(fileNameDTO.iin) : fileNameDTO.iin != null) return false;
        if (quartal != null ? !quartal.equals(fileNameDTO.quartal) : fileNameDTO.quartal != null) return false;
        if (date != null ? !date.equals(fileNameDTO.date) : fileNameDTO.date != null) return false;
        return !(id != null ? !id.equals(fileNameDTO.id) : fileNameDTO.id != null);

    }

    @Override
    public int hashCode() {
        int result = formName != null ? formName.hashCode() : 0;
        result = 31 * result + (iin != null ? iin.hashCode() : 0);
        result = 31 * result + (quartal != null ? quartal.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
