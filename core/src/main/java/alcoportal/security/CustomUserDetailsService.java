package alcoportal.security;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.UserRoleEntity;
import alcoportal.dal.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by good on 03.10.2015.
 */
@Service
@Qualifier("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {

        UserEntity userEntity = userEntityRepository.findByEmailIgnoreCase(username);

        if (userEntity == null) {
            try {
                userEntity = userEntityRepository.findByInn(Long.parseLong(username));
                if (userEntity == null) {
                    throw new UsernameNotFoundException("User not found by email or inn");
                }
            } catch (Exception e) {
                throw new UsernameNotFoundException("User not found by email or inn");
            }
        }

        List<GrantedAuthority> authorities = buildUserAuthority(userEntity.getRoles());

        return buildUserForAuthentication(userEntity, authorities);
    }

    private User buildUserForAuthentication(UserEntity userEntity,
                                            List<GrantedAuthority> authorities) {
        return new User(userEntity.getEmail(), userEntity.getPassword(), authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Collection<UserRoleEntity> userRoleEntities) {

        Set<GrantedAuthority> setAuths = new HashSet<>();

        for (UserRoleEntity userRoleEntity : userRoleEntities) {
            setAuths.add(new SimpleGrantedAuthority(userRoleEntity.getTitle().toString()));
        }

        return new ArrayList<>(setAuths);
    }
}
