package alcoportal.security;

/**
 * Created by good on 15.11.2015.
 */

import alcoportal.dal.domain.entity.BaseEntity;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "remember_me_token")
@SequenceGenerator(sequenceName = "remember_me_token_sequence", name = "default_generator", allocationSize = 1)
public class RememberMeTokenEntity extends BaseEntity {

    private String username;
    private String series;
    private String token;
    private Date date;

    public RememberMeTokenEntity() {
    }

    public RememberMeTokenEntity(PersistentRememberMeToken persistentRememberMeToken) {
        this.username = persistentRememberMeToken.getUsername();
        this.series = persistentRememberMeToken.getSeries();
        this.date = persistentRememberMeToken.getDate();
        this.token = persistentRememberMeToken.getTokenValue();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
