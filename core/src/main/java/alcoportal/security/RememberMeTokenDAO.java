package alcoportal.security;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Repository
@Transactional
public class RememberMeTokenDAO {

    @Autowired
    private RememberMeTokenRepository rememberMeTokenRepository;

    public void createNewToken(RememberMeTokenEntity token) {
        rememberMeTokenRepository.save(token);
    }

    public void updateToken(String series, String tokenValue, Date lastUsed) {

        RememberMeTokenEntity usingToken = rememberMeTokenRepository.findBySeries(series);
        usingToken.setToken(tokenValue);
        usingToken.setDate(lastUsed);
        rememberMeTokenRepository.save(usingToken);
    }

    public RememberMeTokenEntity getTokenForSeries(String seriesId) {
        return rememberMeTokenRepository.findBySeries(seriesId);
    }

    public void removeUserTokens(final String username) {
        rememberMeTokenRepository.deleteByUsername(username);
    }
}