package alcoportal.security;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.userInfo.LicenseEntity;
import alcoportal.dal.domain.entity.userInfo.SubdivisionEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by good on 03.10.2015.
 */

public interface RememberMeTokenRepository extends CrudRepository<RememberMeTokenEntity, Long> {
    public RememberMeTokenEntity findByUsername(String username);
    public RememberMeTokenEntity findBySeries(String series);
    public void deleteByUsername(String username);
}
