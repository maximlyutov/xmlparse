package alcoportal.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by good on 15.11.2015.
 */
@Component
public class CustomRememberMeTokenRepository implements PersistentTokenRepository {

    @Autowired
    private RememberMeTokenDAO tokenDAO;

    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        tokenDAO.createNewToken(new RememberMeTokenEntity(token));
    }

    @Override
    public void updateToken(String series, String tokenValue, Date lastUsed) {
        tokenDAO.updateToken(series, tokenValue, lastUsed);
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        RememberMeTokenEntity token = tokenDAO.getTokenForSeries(seriesId);
        return new PersistentRememberMeToken(token.getUsername(),
                token.getSeries(), token.getToken(), token.getDate());
    }

    @Override
    public void removeUserTokens(String username) {
        tokenDAO.removeUserTokens(username);
    }
}
