package alcoportal.exseptions;

import alcoportal.services.validation.ErrorItem;

import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */

public class ValidateExseption extends Exception {

    List<ErrorItem> errorItems;


    public ValidateExseption(String msg) {
        super(msg);

    }

    public ValidateExseption(String msg, List<ErrorItem> errorItems) {

        super(msg);
        this.errorItems = errorItems;
    }

    public List<ErrorItem> getErrorItems() {
        return errorItems;
    }

    public ValidateExseption setErrorItems(List<ErrorItem> errorItems) {
        this.errorItems = errorItems;
        return this;
    }
}
