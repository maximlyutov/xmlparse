package alcoportal.exseptions;

import org.xml.sax.SAXException;

/**
 * Created by vladimir akummail@gmail.com on 10/6/15.
 */
public class ValidateSAXException extends SAXException {
    private String elementName;

    public ValidateSAXException(String msg, String elementName) {

        super(msg);
        this.elementName = elementName;
    }

    public String getElementName() {
        return elementName;
    }
}
