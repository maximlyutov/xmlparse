package alcoportal.exseptions;

/**
 * Created by vladimir akummail@gmail.com on 11/1/15.
 */
public class ParsingExseption extends Exception{
    public ParsingExseption() {
    }

    public ParsingExseption(String s) {
        super(s);
    }

    public ParsingExseption(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ParsingExseption(Throwable throwable) {
        super(throwable);
    }
}
