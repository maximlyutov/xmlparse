package alcoportal.services.parsing;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.exseptions.ParsingExseption;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created by vladimir akummail@gmail.com on 10/22/15.
 */
public interface Parsing {
    void parse(DocumentHistoryEntity documentHistoryEntity) throws ParsingExseption;
}
