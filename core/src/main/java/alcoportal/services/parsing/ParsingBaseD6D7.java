package alcoportal.services.parsing;

import alcoportal.dal.domain.entity.D6.ProducerEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.util.HashMap;

/**
 * Created by vladimir akummail@gmail.com on 11/21/15.
 */
public class ParsingBaseD6D7 extends ParsingBase {

    protected HashMap<String, ProducerEntity> getProducers(Document document, DocumentHistoryEntity documentHistoryEntity) throws ParseException {
        HashMap<String, ProducerEntity> producerDTOHashMap = new HashMap<>();

        NodeList producers = document.getElementsByTagName("ПроизводителиИмпортеры");
        log.info("Парсим производителей");
        log.info("Количество производителей :" + producers.getLength());
        for (int i = 0; i < producers.getLength(); i++) {
            Element producer = (Element) producers.item(i);

            ProducerEntity producerEntity = new ProducerEntity();
            producerEntity.setDocumentHistory(documentHistoryEntity);
            String id = producer.getAttribute("ИдПроизвИмп");
            producerEntity.setInn(producer.getAttribute("П000000000005"));
            producerEntity.setKpp(producer.getAttribute("П000000000006"));
            producerDTOHashMap.put(id, producerEntity);
        }
        return producerDTOHashMap;
    }
}
