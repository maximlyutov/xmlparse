package alcoportal.services.parsing;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.repository.KontragentRepository;
import alcoportal.dal.repository.ProducerRepository;
import alcoportal.dal.repository.TurnoverD5Repository;
import alcoportal.dal.repository.TurnoverRepository;
import alcoportal.exseptions.ParsingExseption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by vladimir akummail@gmail.com on 10/21/15.
 */
@Service
public class ParsingService {

    @Autowired
    ProducerRepository producerRepository;

    @Autowired
    KontragentRepository kontragentRepository;

    @Autowired
    TurnoverRepository turnoverRepository;

    @Autowired
    TurnoverD5Repository turnoverD5Repository;

    public void parsing(DocumentHistoryEntity documentHistoryEntity) throws ParsingExseption {
        Parsing parsing = parsingFactory(documentHistoryEntity);
        parsing.parse(documentHistoryEntity);
    }

    private Parsing parsingFactory(DocumentHistoryEntity documentHistoryEntity) throws ParsingExseption {
        switch (documentHistoryEntity.getFormName()) {
            case "D5":
                return new ParsingD5(turnoverD5Repository);
            case "D6":
                return new ParsingD6(producerRepository, kontragentRepository, turnoverRepository);
            case "D7":
                return new ParsingD7(producerRepository, kontragentRepository, turnoverRepository);
            default:
                throw new ParsingExseption("Неизвестная форма: " + documentHistoryEntity.getFormName());
        }
    }
}
