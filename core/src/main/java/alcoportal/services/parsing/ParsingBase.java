package alcoportal.services.parsing;

import alcoportal.dal.domain.entity.D6.ProducerEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.HashMap;

/**
 * Created by vladimir akummail@gmail.com on 10/21/15.
 */
public class ParsingBase {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public Document openDocument(byte[] bytes) throws IOException, SAXException, ParserConfigurationException {
        log.info("Открытие xml и нормализация для парсинга.");
        InputStream file = new ByteArrayInputStream(bytes);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);
        doc.getDocumentElement().normalize();
        return doc;
    }

    protected Element getElementByName(Element currentNode, String elementName) {
        NodeList urNode = currentNode.getElementsByTagName(elementName);
        return (Element) urNode.item(0);
    }


}
