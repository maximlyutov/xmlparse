package alcoportal.services.parsing;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.TurnoverD5;
import alcoportal.dal.repository.TurnoverD5Repository;
import alcoportal.exseptions.ParsingExseption;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created by vladimir akummail@gmail.com on 11/21/15.
 */
public class ParsingD5 extends ParsingBase implements Parsing {

    private TurnoverD5Repository turnoverD5Repository;

    public ParsingD5(TurnoverD5Repository turnoverD5Repository) {
        this.turnoverD5Repository = turnoverD5Repository;
    }

    @Override
    public void parse(DocumentHistoryEntity documentHistoryEntity) throws ParsingExseption {
        try {
            log.info("Парсим документ D5");
            log.info("Разбираем основной документ");
            Document document = openDocument(documentHistoryEntity.getDocument());
            NodeList turnovers = document.getElementsByTagName("Оборот");

            for (int turnoversIndex = 0; turnoversIndex < turnovers.getLength(); turnoversIndex++) {

                Element turnover = (Element) turnovers.item(turnoversIndex);

                String productCode = turnover.getAttribute("П000000000003");

                NodeList producers = turnover.getElementsByTagName("СведПроизвИмпорт");
                for (int producersIndex = 0; producersIndex < producers.getLength(); producersIndex++) {
                    try {
                        Element producer = (Element) producers.item(producersIndex);

                        String inn = producer.getAttribute("П000000000005");
                        String kpp = producer.getAttribute("П000000000006");

                        double releaseVolume = Double.parseDouble(producer.getAttribute("П000000000015"));
                        double releaseOtherVolume = Double.parseDouble(producer.getAttribute("П000000000013"));
                        double releasereturnKontragentVolume = Double.parseDouble(producer.getAttribute("П000000000012"));

                        double consumptionVolume = Double.parseDouble(producer.getAttribute("П000000000023"));
                        double consumptionOtherVolume = Double.parseDouble(producer.getAttribute("П000000000020"));
                        double consumptionReturnKontragentVolume = Double.parseDouble(producer.getAttribute("П000000000021"));

                        TurnoverD5 tornoverD5 = new TurnoverD5()
                                .setProductCode(productCode)
                                .setInn(inn)
                                .setKpp(kpp)
                                .setReleaseVolume(releaseVolume)
                                .setReleaseOtherVolume(releaseOtherVolume)
                                .setReleaseReturnKontragentVolume(releasereturnKontragentVolume)
                                .setConsumptionVolume(consumptionVolume)
                                .setConsumptionOherVolume(consumptionOtherVolume)
                                .setConsumptionReturnKontragentVolume(consumptionReturnKontragentVolume)
                                .setDocumentHistory(documentHistoryEntity);

                        turnoverD5Repository.save(tornoverD5);
                    } catch (Exception e) {
                        log.error("Ошибка парсинга производителей оборот индекс " + turnoversIndex + " производитель индекс " + producersIndex);
                        throw e;
                    }

                }
            }

        } catch (Exception e) {
            log.error("Ошибка парсинга файла", e);
            throw new ParsingExseption("Ошибка парсинга файла");
        } finally {
            log.info("Закончили парсить документ D5");
        }
    }
}
