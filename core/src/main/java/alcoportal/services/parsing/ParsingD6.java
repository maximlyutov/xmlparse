package alcoportal.services.parsing;

import alcoportal.dal.domain.entity.D6.KontragentEntity;
import alcoportal.dal.domain.entity.D6.ProducerEntity;
import alcoportal.dal.domain.entity.D6.TurnoverEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.other.TurnoverType;
import alcoportal.dal.repository.KontragentRepository;
import alcoportal.dal.repository.ProducerRepository;
import alcoportal.dal.repository.TurnoverRepository;
import alcoportal.exseptions.ParsingExseption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by vladimir akummail@gmail.com on 10/21/15.
 */

public class ParsingD6 extends ParsingBaseD6D7 implements Parsing {

    private ProducerRepository producerRepository;

    private KontragentRepository kontragentRepository;

    private TurnoverRepository turnoverRepository;

    public ParsingD6(ProducerRepository producerRepository, KontragentRepository kontragentRepository, TurnoverRepository turnoverRepository) {
        this.producerRepository = producerRepository;
        this.kontragentRepository = kontragentRepository;
        this.turnoverRepository = turnoverRepository;
    }

    @Override
    public void parse(DocumentHistoryEntity documentHistoryEntity) throws ParsingExseption {
        try {
            log.info("Парсим документ D6");
            Document document = openDocument(documentHistoryEntity.getDocument());

            HashMap<String, KontragentEntity> kontragentDTOHashMap = getAgents(document, documentHistoryEntity);
            log.info("Пишем в бд контрогентов");
            kontragentDTOHashMap.forEach(
                    (s, kontragentEntity) ->
                            kontragentRepository.save(kontragentEntity));
            HashMap<String, ProducerEntity> producerDTOHashMap = getProducers(document, documentHistoryEntity);
            log.info("Пишем в бд производителей");

            producerDTOHashMap.forEach(
                    (s, producerEntity) ->
                            producerRepository.save(producerEntity));

            log.info("Разбираем основной документ");

            NodeList volumeturnovers = document.getElementsByTagName("ОбъемОборота");
            for (int volumeturnoversIndex = 0; volumeturnoversIndex < volumeturnovers.getLength(); volumeturnoversIndex++) {
                Element volumeturnover = (Element) volumeturnovers.item(volumeturnoversIndex);

                if (!Boolean.parseBoolean(volumeturnover.getAttribute("НаличиеПоставки")) && !Boolean.parseBoolean(volumeturnover.getAttribute("НаличиеВозврата"))) {
                    continue;
                }

                String volumeturnoverKpp = volumeturnover.getAttribute("КППЮЛ");
                NodeList turnovers = volumeturnover.getElementsByTagName("Оборот");


                for (int turnoversIndex = 0; turnoversIndex < turnovers.getLength(); turnoversIndex++) {
                    Element turnover = (Element) turnovers.item(turnoversIndex);

                    String productCode = turnover.getAttribute("П000000000003");

                    NodeList producers = turnover.getElementsByTagName("СведПроизвИмпорт");
                    for (int producersIndex = 0; producersIndex < producers.getLength(); producersIndex++) {
                        Element producer = (Element) producers.item(producersIndex);
                        ProducerEntity producerEntity = producerDTOHashMap.get(producer.getAttribute("ИдПроизвИмп"));

                        NodeList kontragents = producer.getElementsByTagName("Получатель");
                        for (int kontragentsIndex = 0; kontragentsIndex < kontragents.getLength(); kontragentsIndex++) {
                            Element kontragent = (Element) kontragents.item(kontragentsIndex);
                            KontragentEntity kontragentEntity = kontragentDTOHashMap.get(kontragent.getAttribute("ИдПолучателя"));

                            NodeList deliverys = kontragent.getElementsByTagName("Поставка");
                            if (deliverys.getLength() > 0) //Поставка
                            {
                                for (int deliverysIndex = 0; deliverysIndex < deliverys.getLength(); deliverysIndex++) {
                                    Element delivery = (Element) deliverys.item(deliverysIndex);
                                    turnoverRepository.save(
                                            getTurnoverEntity(productCode, producerEntity, kontragentEntity, documentHistoryEntity, delivery)
                                                    .setType(TurnoverType.SUPPLY).setKpp(volumeturnoverKpp));
                                }
                            } else //Возврат
                            {
                                NodeList refunds = kontragent.getElementsByTagName("Возврат");
                                for (int refundsIndex = 0; refundsIndex < refunds.getLength(); refundsIndex++) {
                                    Element refund = (Element) refunds.item(refundsIndex);
                                    turnoverRepository.save(
                                            getTurnoverEntity(productCode, producerEntity, kontragentEntity, documentHistoryEntity, refund)
                                                    .setType(TurnoverType.RETURN).setKpp(volumeturnoverKpp));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Ошибка парсинга файла", e);
            throw new ParsingExseption("Ошибка парсинга файла");
        }finally {
            log.info("Закончили парсить документ D6");
        }
    }

    protected TurnoverEntity getTurnoverEntity(String productCode, ProducerEntity producerEntity, KontragentEntity kontragentEntity, DocumentHistoryEntity documentHistoryEntity, Element delivery) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
        TurnoverEntity turnoverEntity = new TurnoverEntity();
        turnoverEntity.setProductCode(productCode);
        turnoverEntity.setPurchaseDate(format.parse(delivery.getAttribute("П000000000018")));
        turnoverEntity.setNumberTTN(delivery.getAttribute("П000000000019"));
        turnoverEntity.setNumberGTD(delivery.getAttribute("П000000000020"));
        turnoverEntity.setVolume(Double.parseDouble(delivery.getAttribute("П000000000021")));
        turnoverEntity.setProducer(producerEntity);
        turnoverEntity.setKontragent(kontragentEntity);
        turnoverEntity.setDocumentHistory(documentHistoryEntity);
        return turnoverEntity;
    }

    protected HashMap<String, KontragentEntity> getAgents(Document document, DocumentHistoryEntity documentHistoryEntity) throws ParseException {
        HashMap<String, KontragentEntity> kontragentDTOHashMap = new HashMap<>();

        NodeList agents = document.getElementsByTagName("Контрагенты");
        log.info("Парсим контрагентов");
        log.info("Количество контрагентов :" + agents.getLength());
        for (int i = 0; i < agents.getLength(); i++) {
            Element agent = (Element) agents.item(i);
            String id = agent.getAttribute("ИдКонтр");
            KontragentEntity kontragentEntity = new KontragentEntity();
            kontragentEntity.setDocumentHistoryEntity(documentHistoryEntity);
            Element ur = getElementByName(agent, "ЮЛ");
            kontragentEntity.setInn(ur.getAttribute("П000000000009"));
            kontragentEntity.setKpp(ur.getAttribute("П000000000010"));
            Element license = getElementByName(agent, "Лицензия");
            kontragentEntity.setLicenseNumber(license.getAttribute("П000000000011"));
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
            kontragentEntity.setStartDateLicensing(format.parse(license.getAttribute("П000000000012")));
            kontragentEntity.setExpirationDateLicense(format.parse(license.getAttribute("П000000000013")));
            kontragentDTOHashMap.put(id, kontragentEntity);
        }
        return kontragentDTOHashMap;
    }

}
