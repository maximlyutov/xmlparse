package alcoportal.services.revise;

import alcoportal.dal.repository.*;

/**
 * Created by vladimir akummail@gmail.com on 11/4/15.
 */
public class ReviseD7 extends ReviseD6D7Base {

    public ReviseD7(
            ReviseRepository reviseRepository,
            DocumentHistoryRepository documentHistoryRepository,
            KontragentRepository kontragentRepository,
            TurnoverRepository turnoverRepository,
            ReviseLogRepository reviseLogRepository,
            ReviseLogCustomRepositoryImpl reviseLogCustomRepository) {
        super(reviseRepository,
                documentHistoryRepository,
                kontragentRepository,
                turnoverRepository,
                reviseLogRepository,
                reviseLogCustomRepository);
    }

    @Override
    String getFindDocumentName() {
        return "D6";
    }
}
