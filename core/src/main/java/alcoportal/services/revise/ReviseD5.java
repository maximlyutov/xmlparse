package alcoportal.services.revise;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.ProducerTurnoverEntity;
import alcoportal.dal.domain.entity.TurnoverD5;
import alcoportal.dal.domain.entity.revise.ReviseD5LogsEntity;
import alcoportal.dal.domain.entity.revise.ReviseEntity;
import alcoportal.dal.repository.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by vladimir akummail@gmail.com on 11/21/15.
 */
public class ReviseD5 implements Revise {
    Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    ReviseRepository reviseRepository;
    DocumentHistoryRepository documentHistoryRepository;
    ReviseD5LogRepository reviseD5LogRepository;
    TurnoverRepository turnoverRepository;
    TurnoverD5Repository turnoverD5Repository;
    ProducerRepository producerRepository;
    TurnoverCustomRepository turnoverCustomRepository;

    public ReviseD5(
            ReviseRepository reviseRepository,
            DocumentHistoryRepository documentHistoryRepository,
            ReviseD5LogRepository reviseD5LogRepository,
            TurnoverRepository turnoverRepository,
            TurnoverD5Repository turnoverD5Repository,
            ProducerRepository producerRepository,
            TurnoverCustomRepository turnoverCustomRepository) {

        this.reviseRepository = reviseRepository;
        this.documentHistoryRepository = documentHistoryRepository;
        this.reviseD5LogRepository = reviseD5LogRepository;
        this.turnoverRepository = turnoverRepository;
        this.turnoverD5Repository = turnoverD5Repository;
        this.producerRepository = producerRepository;
        this.turnoverCustomRepository = turnoverCustomRepository;
    }

    @Override
    public boolean buildRevise(ReviseEntity reviseEntity) {

        DocumentHistoryEntity documentHistoryEntity = reviseEntity.getDocumentHistory();

        DocumentHistoryEntity documentHistory6D = documentHistoryRepository
                .findOneByUserAndFormNameAndYearAndQuarterAndFullReviseStateTrue(
                        documentHistoryEntity.getUser(),
                        "D6",
                        documentHistoryEntity.getYear(),
                        documentHistoryEntity.getQuarter());

        DocumentHistoryEntity documentHistory7D = documentHistoryRepository
                .findOneByUserAndFormNameAndYearAndQuarterAndFullReviseStateTrue(
                        documentHistoryEntity.getUser(),
                        "D7",
                        documentHistoryEntity.getYear(),
                        documentHistoryEntity.getQuarter());

        if (documentHistory6D == null || documentHistory7D == null) {
            reviseEntity.setDescription("Не найдены формы D6 или D7");
            return false;
        }

        logger.info(String.format("Сверка формы D5:%d c D6:%d D7:%d", reviseEntity.getDocumentHistory().getId(), documentHistory6D.getId(), documentHistory7D.getId()));


        ///Загрузка данных из 5 формы

        for (TurnoverD5 turnoverD5 : turnoverD5Repository.findAllByDocumentHistory(documentHistoryEntity)) {
            reviseD5LogRepository.save(new ReviseD5LogsEntity()
                    .setRevise(reviseEntity)
                    .setProductCode(turnoverD5.getProductCode())
                    .setProducerInn(turnoverD5.getInn())
                    .setProducerKpp(turnoverD5.getKpp())
                    .setReleaseVolume(turnoverD5.getReleaseVolume())
                    .setReleaseOtherVolume(turnoverD5.getReleaseOtherVolume())
                    .setReleaseReturnKontragentVolume(turnoverD5.getReleaseReturnKontragentVolume())
                    .setConsumptionVolume(turnoverD5.getConsumptionVolume())
                    .setConsumptionOherVolume(turnoverD5.getConsumptionOherVolume())
                    .setConsumptionReturnKontragentVolume(turnoverD5.getConsumptionReturnKontragentVolume()));
        }

        ////

///Загрузка данных из 6 формы
        {
            List<ProducerTurnoverEntity> producerTurnover6Entities = turnoverCustomRepository.getTurnoversForReviseD5(documentHistory6D.getId());

            for (ProducerTurnoverEntity turnoverEntity : producerTurnover6Entities) {
                if (turnoverEntity.getProductCode() == null) {
                    continue;
                }
                ReviseD5LogsEntity d5LogsEntity = reviseD5LogRepository
                        .findOneByReviseAndProductCodeAndProducerInnAndProducerKpp(
                                reviseEntity,
                                turnoverEntity.getProductCode(),
                                turnoverEntity.getProducer().getInn(),
                                turnoverEntity.getProducer().getKpp());

                if (d5LogsEntity == null) {
                    reviseD5LogRepository.save(createReviseD5LogsEntity(turnoverEntity, reviseEntity)
                            .setReleaseD6Volume(turnoverEntity.getVolume())
                            .setReleaseReturnD6Volume(turnoverEntity.getReturnVolume()));

                } else // if (d5LogsEntity.getReleaseVolume() != turnoverEntity.getVolume())
                {
                    reviseD5LogRepository.save(d5LogsEntity
                            .setReleaseD6Volume(turnoverEntity.getVolume())
                            .setReleaseReturnD6Volume(turnoverEntity.getReturnVolume())
                            .setReleaseDelta(d5LogsEntity.getReleaseVolume() - turnoverEntity.getVolume()));
                }
            }
            ////

///Загрузка данных из 7 формы
            {
                List<ProducerTurnoverEntity> producerTurnover7Entities = turnoverCustomRepository.getTurnoversForReviseD5(documentHistory7D.getId());

                for (ProducerTurnoverEntity turnoverEntity : producerTurnover7Entities) {
                    if (turnoverEntity.getProductCode() == null) {
                        continue;
                    }
                    ReviseD5LogsEntity d5LogsEntity = reviseD5LogRepository
                            .findOneByReviseAndProductCodeAndProducerInnAndProducerKpp(
                                    reviseEntity,
                                    turnoverEntity.getProductCode(),
                                    turnoverEntity.getProducer().getInn(),
                                    turnoverEntity.getProducer().getKpp());

                    if (d5LogsEntity == null) {
                        reviseD5LogRepository.save(createReviseD5LogsEntity(turnoverEntity, reviseEntity)
                                .setConsumptionD7Volume(turnoverEntity.getVolume())
                                .setConsumptionReturnD7Volume(turnoverEntity.getReturnVolume()));

                    } else // if (d5LogsEntity.getReleaseVolume() != turnoverEntity.getVolume())
                    {
                        reviseD5LogRepository.save(d5LogsEntity
                                .setConsumptionD7Volume(turnoverEntity.getVolume())
                                .setConsumptionReturnD7Volume(turnoverEntity.getReturnVolume()))
                                .setConsumptionDelta(d5LogsEntity.getReleaseVolume() - turnoverEntity.getVolume());
                        //  }
                    }
                }
                ////
            }
            if (reviseD5LogRepository.getCountForReviseId(reviseEntity.getId()) > 0) {
                return false;
            }

            return true;
        }
    }

    private ReviseD5LogsEntity createReviseD5LogsEntity(ProducerTurnoverEntity turnoverEntity, ReviseEntity reviseEntity) {
        return new ReviseD5LogsEntity()
                .setProductCode(turnoverEntity.getProductCode())
                .setProducerInn(turnoverEntity.getProducer().getInn())
                .setProducerKpp(turnoverEntity.getProducer().getKpp())
                .setRevise(reviseEntity);
    }
}
