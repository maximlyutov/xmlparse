package alcoportal.services.revise;

import alcoportal.dal.domain.entity.revise.ReviseEntity;

/**
 * Created by vladimir akummail@gmail.com on 11/4/15.
 */
public interface Revise {
    boolean buildRevise(ReviseEntity reviseEntity);
}
