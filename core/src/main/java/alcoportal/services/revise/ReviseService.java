package alcoportal.services.revise;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.revise.ReviseEntity;
import alcoportal.dal.domain.entity.revise.ReviseLogEntity;
import alcoportal.dal.domain.entity.revise.ReviseState;
import alcoportal.dal.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 11/3/15.
 */
@Service
public class ReviseService {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ReviseRepository reviseRepository;

    @Autowired
    DocumentHistoryRepository documentHistoryRepository;

    @Autowired
    KontragentRepository kontragentRepository;

    @Autowired
    TurnoverRepository turnoverRepository;

    @Autowired
    ReviseLogRepository reviseLogRepository;

    @Autowired
    ReviseD5LogRepository reviseD5LogRepository;

    @Autowired
    TurnoverD5Repository turnoverD5Repository;

    @Autowired
    TurnoverCustomRepository turnoverCustomRepository;

    @Autowired
    ProducerRepository producerRepository;

    @Autowired
    ReviseLogCustomRepositoryImpl reviseLogCustomRepository;

    public void fullRevise(Long documentHistoryId, boolean reviceState, UserEntity currentUser) {

        DocumentHistoryEntity documentHistoryEntity = documentHistoryRepository.findOne(documentHistoryId);

        if (reviceState) {
            createFullRevise(currentUser, documentHistoryEntity);
            documentHistoryEntity.setAlreadyRevised(true);
            documentHistoryRepository.save(documentHistoryEntity);
        } else {
            deleteReviseAndReviseLog(documentHistoryEntity);
        }
    }

    private void createFullRevise(UserEntity currentUser, DocumentHistoryEntity documentHistoryEntity) {
        if (documentHistoryEntity.isValidateState()) {
            deletPreviousFullRevise(documentHistoryEntity);

            ReviseEntity reviseEntity = new ReviseEntity();
            reviseEntity.setDocumentHistory(documentHistoryEntity)
                    .setCreateAt(new Date())
                    .setReviseState(ReviseState.IN_PROGRESS);

            reviseRepository.save(reviseEntity);
            documentHistoryRepository.save(documentHistoryEntity.setFullReviseState(true));

            /// todo запускать в новом потоке если не будеть быстро работать
            startRevise(reviseEntity);
        }
    }


    private void deleteReviseAndReviseLog(DocumentHistoryEntity documentHistoryEntity) {
        if (documentHistoryEntity == null) {
            return;
        }
        documentHistoryEntity.setFullReviseState(false);
        documentHistoryRepository.save(documentHistoryEntity);
        ReviseEntity reviseEntity = reviseRepository.findOneByDocumentHistory(documentHistoryEntity);
        if (reviseEntity == null) {
            return;
        }
        reviseRepository.delete(reviseEntity);
    }

    private void deletPreviousFullRevise(DocumentHistoryEntity documentHistoryEntity) {
        DocumentHistoryEntity previousDocumentHistory = documentHistoryRepository.findOneByUserAndFormNameAndYearAndQuarterAndFullReviseStateTrue(
                documentHistoryEntity.getUser(),
                documentHistoryEntity.getFormName(),
                documentHistoryEntity.getYear(),
                documentHistoryEntity.getQuarter());

        deleteReviseAndReviseLog(previousDocumentHistory);
    }

    public List<ReviseEntity> getRevises(UserEntity currentUser) {
        List<ReviseEntity> result = reviseRepository.getRevises(currentUser.getId());

        for (ReviseEntity reviseEntity : result) {
            reviseEntity.setReviseLogs(reviseLogRepository.finAllByReviseId(reviseEntity.getId()));
            reviseEntity.setReviseD5Logs(reviseD5LogRepository.finAllByReviseId(reviseEntity.getId()));
        }

        return result;
    }

    private void startRevise(ReviseEntity reviseEntity) {

        try {
            Revise revise = reviseFactory(reviseEntity);
            if (revise.buildRevise(reviseEntity)) {
                reviseEntity.setReviseState(ReviseState.SUCCESS);
            } else {
                reviseEntity.setReviseState(ReviseState.ERROR);
            }
        } catch (Exception e) {
            reviseEntity.setReviseState(ReviseState.ERROR);
            log.error("Ошибка сверки", e);
        }

        reviseRepository.save(reviseEntity);
    }

    private Revise reviseFactory(ReviseEntity reviseEntity) throws Exception {
        switch (reviseEntity.getDocumentHistory().getFormName()) {
            case "D5":
                return new ReviseD5(
                        reviseRepository,
                        documentHistoryRepository,
                        reviseD5LogRepository,
                        turnoverRepository,
                        turnoverD5Repository,
                        producerRepository,
                        turnoverCustomRepository);
            case "D6":
                return new ReviseD6(
                        reviseRepository,
                        documentHistoryRepository,
                        kontragentRepository,
                        turnoverRepository,
                        reviseLogRepository,
                        reviseLogCustomRepository);
            case "D7":
                return new ReviseD7(
                        reviseRepository,
                        documentHistoryRepository,
                        kontragentRepository,
                        turnoverRepository,
                        reviseLogRepository,
                        reviseLogCustomRepository);
            default:
                throw new Exception("Неизвестный вид формы: " + reviseEntity.getDocumentHistory().getFormName());
        }
    }

    public void removeRevise(Long documentHistoryId, UserEntity currentUser) {
        DocumentHistoryEntity documentHistoryEntity = documentHistoryRepository.findOne(documentHistoryId);
        if (documentHistoryEntity.getUser().getEmail() == currentUser.getEmail()) {
            documentHistoryRepository.delete(documentHistoryId);
        }
    }
}
