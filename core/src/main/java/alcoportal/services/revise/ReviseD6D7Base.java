package alcoportal.services.revise;

import alcoportal.dal.domain.entity.D6.KontragentEntity;
import alcoportal.dal.domain.entity.D6.TurnoverEntity;
import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.revise.ReviseEntity;
import alcoportal.dal.domain.entity.revise.ReviseLogEntity;
import alcoportal.dal.repository.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by vladimir akummail@gmail.com on 11/15/15.
 */
public abstract class ReviseD6D7Base implements Revise {
    Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    ReviseRepository reviseRepository;

    DocumentHistoryRepository documentHistoryRepository;

    KontragentRepository kontragentRepository;

    TurnoverRepository turnoverRepository;

    ReviseLogRepository reviseLogRepository;

    ReviseLogCustomRepositoryImpl reviseLogCustomRepository;


    public ReviseD6D7Base(
            ReviseRepository reviseRepository,
            DocumentHistoryRepository documentHistoryRepository,
            KontragentRepository kontragentRepository,
            TurnoverRepository turnoverRepository,
            ReviseLogRepository reviseLogRepository,
            ReviseLogCustomRepositoryImpl reviseLogCustomRepository) {
        this.reviseRepository = reviseRepository;
        this.documentHistoryRepository = documentHistoryRepository;
        this.kontragentRepository = kontragentRepository;
        this.turnoverRepository = turnoverRepository;
        this.reviseLogRepository = reviseLogRepository;
        this.reviseLogCustomRepository = reviseLogCustomRepository;
    }

    @Override
    public boolean buildRevise(ReviseEntity reviseEntity) {
        DocumentHistoryEntity documentHistoryEntity = reviseEntity.getDocumentHistory();
        logger.info("Start revise id: " + documentHistoryEntity.getId() + " name:" + documentHistoryEntity.getFormName() + " file:" + documentHistoryEntity.getFileName());
        List<DocumentHistoryEntity> documentHistoryEntities = documentHistoryRepository
                .findAllByIdNotAndFormNameAndYearAndQuarterAndFullReviseStateTrueAndUserNot(
                        documentHistoryEntity.getId(),
                        getFindDocumentName(),
                        documentHistoryEntity.getYear(),
                        documentHistoryEntity.getQuarter(),
                        documentHistoryEntity.getUser());


        logger.info("revise with:");
        for (DocumentHistoryEntity logItem : documentHistoryEntities) {
            logger.info("id: " + logItem.getId() + " name: " + logItem.getFormName() + " file: " + logItem.getFileName());
        }

        List<KontragentEntity> kontragentEntityListFull = new ArrayList<>();

        for (DocumentHistoryEntity itemDocumen : documentHistoryEntities) {
            List<KontragentEntity> kontragentEntityList = kontragentRepository.getKontragentsNew(documentHistoryEntity.getId(), itemDocumen.getUser().getInn().toString()); //все наши поставщики которые есть в системе
            kontragentEntityListFull.addAll(kontragentEntityList);
        }

        logger.info("My kontragents : " + kontragentEntityListFull.size());

        List<TurnoverEntity> turnoverEntitiesMy = turnoverRepository.findAllByKontragentIn(kontragentEntityListFull); //все обороты наших поставщиков
        logger.info("My turnover : " + turnoverEntitiesMy.size());

        for (DocumentHistoryEntity itemDocumen : documentHistoryEntities) {
            List<String> turnovers = turnoverRepository.getAllKpp(documentHistoryEntity.getId());
            if (turnovers.isEmpty()) {
                kontragentEntityListFull = new ArrayList<>();
            } else {
                kontragentEntityListFull = kontragentRepository.getKontragentsNew(itemDocumen.getId(), documentHistoryEntity.getUser().getInn().toString(), turnovers); //все наши обороты
            }
        }

        if (!(kontragentEntityListFull.size() > 0)) {
            return true;
        }

        logger.info("Them kontragents : " + kontragentEntityListFull.size());
        List<ReviseLogEntity> reviseLogEntities = saveRevisiLogsUr1(reviseEntity, turnoverEntitiesMy);

        List<TurnoverEntity> turnoverEntities = turnoverRepository.findAllByKontragentIn(kontragentEntityListFull);
        logger.info("Them turnover : " + turnoverEntities.size());
        if (reviseLogEntities.size() > 0) {
            for (TurnoverEntity turnoverEntity : turnoverEntities) {

                ReviseLogEntity reviseLogEntity = reviseLogCustomRepository
                        .findOneBy(
                                reviseEntity,
                                turnoverEntity.getProductCode(),
                                turnoverEntity.getProducer().getInn(),
                                turnoverEntity.getProducer().getKpp(),
                                turnoverEntity.getNumberTTN(),
                                turnoverEntity.getNumberGTD(),
                                turnoverEntity.getPurchaseDate(),
                                turnoverEntity.getType(),
                                turnoverEntity.getDocumentHistory().getUser().getInn().toString(),
                                turnoverEntity.getKpp()
                        );

                if (reviseLogEntity != null) {
                    if (reviseLogEntity.getVolume() == turnoverEntity.getVolume()) {
                        reviseLogRepository.delete(reviseLogEntity);
                        continue;
                    }
                }
                ReviseLogEntity reviseLogEntityToSave = makeUr2ReviseLogEntity(reviseLogEntity, reviseEntity, turnoverEntity);
                reviseLogRepository.save(reviseLogEntityToSave);
            }

        } else {
            saveReviseLogsur2(reviseEntity, turnoverEntities);
        }

        return !(reviseLogRepository.findAllByRevise(reviseEntity).size() > 0);
    }

    abstract String getFindDocumentName();

    private List<ReviseLogEntity> saveRevisiLogsUr1(ReviseEntity reviseEntity, List<TurnoverEntity> turnoverEntitiesMy) {
        List<ReviseLogEntity> reviseEntities = new ArrayList<>();
        for (TurnoverEntity turnoverEntity : turnoverEntitiesMy) {
            ReviseLogEntity reviseLogEntity = makeReviseLogEntity(reviseEntity, turnoverEntity);

            reviseLogRepository.save(reviseLogEntity);
            reviseEntities.add(reviseLogEntity);
        }
        return reviseEntities;
    }

    private void saveReviseLogsur2(ReviseEntity reviseEntity, List<TurnoverEntity> turnoverEntitiesMy) {
        for (TurnoverEntity turnoverEntity : turnoverEntitiesMy) {
            reviseLogRepository.save(makeUr2ReviseLogEntity(null, reviseEntity, turnoverEntity));
        }
    }

    private ReviseLogEntity makeUr2ReviseLogEntity(ReviseLogEntity reviseLogEntity, ReviseEntity reviseEntity, TurnoverEntity turnoverEntity) {
        if (reviseLogEntity == null) {
            reviseLogEntity = new ReviseLogEntity();
            reviseLogEntity.setProductCode(turnoverEntity.getProductCode());
            reviseLogEntity.setProducerInn(turnoverEntity.getProducer().getInn());
            reviseLogEntity.setProducerKpp(turnoverEntity.getProducer().getKpp());
            reviseLogEntity.setProductCode(turnoverEntity.getProductCode());
            reviseLogEntity.setType(turnoverEntity.getType());
            reviseLogEntity.setInn(turnoverEntity.getKontragent().getInn());
            reviseLogEntity.setKpp(turnoverEntity.getKontragent().getKpp());
//            reviseLogEntity.setProducerInn(turnoverEntity.getProducer().getInn());
//            reviseLogEntity.setProducerKpp(turnoverEntity.getProducer().getKpp());
//            reviseLogEntity.setNumberTTN()
        }
        else {
            reviseLogEntity.setDelta(reviseLogEntity.getVolume() - reviseLogEntity.getUr2Volume());
        }

        reviseLogEntity.setUr2Date(turnoverEntity.getPurchaseDate());
        reviseLogEntity.setUr2NumberGTD(turnoverEntity.getNumberGTD());
        reviseLogEntity.setUr2NumberTTN(turnoverEntity.getNumberTTN());
        reviseLogEntity.setUr2Type(turnoverEntity.getType());
        reviseLogEntity.setUr2ProductCode(turnoverEntity.getProductCode());
        reviseLogEntity.setUr2ProducerInn(turnoverEntity.getProducer().getInn());
        reviseLogEntity.setUr2ProducerKpp(turnoverEntity.getProducer().getKpp());
        reviseLogEntity.setUr2Volume(turnoverEntity.getVolume());
        reviseLogEntity.setUr2KontragentInn(turnoverEntity.getKontragent().getInn());
        reviseLogEntity.setUr2KontragentKpp(turnoverEntity.getKontragent().getKpp());
        reviseLogEntity.setUr2inn(turnoverEntity.getDocumentHistory().getUser().getInn().toString());
        reviseLogEntity.setUr2kpp(turnoverEntity.getKpp());
        reviseLogEntity.setKontragentInn(turnoverEntity.getDocumentHistory().getUser().getInn().toString());
        reviseLogEntity.setKontragentKpp(turnoverEntity.getKpp());
        reviseLogEntity.setRevise(reviseEntity);

        return reviseLogEntity;
    }

    private ReviseLogEntity makeReviseLogEntity(ReviseEntity reviseEntity, TurnoverEntity turnoverEntity) {
        ReviseLogEntity reviseLogEntity = new ReviseLogEntity();
        reviseLogEntity.setDate(turnoverEntity.getPurchaseDate());
        reviseLogEntity.setNumberGTD(turnoverEntity.getNumberGTD());
        reviseLogEntity.setNumberTTN(turnoverEntity.getNumberTTN());
        reviseLogEntity.setType(turnoverEntity.getType());
        reviseLogEntity.setProductCode(turnoverEntity.getProductCode());
        reviseLogEntity.setProducerInn(turnoverEntity.getProducer().getInn());
        reviseLogEntity.setProducerKpp(turnoverEntity.getProducer().getKpp());
        reviseLogEntity.setKontragentInn(turnoverEntity.getKontragent().getInn());
        reviseLogEntity.setKontragentKpp(turnoverEntity.getKontragent().getKpp());
        reviseLogEntity.setVolume(turnoverEntity.getVolume());
        reviseLogEntity.setInn(turnoverEntity.getDocumentHistory().getUser().getInn().toString());
        reviseLogEntity.setKpp(turnoverEntity.getKpp());
        reviseLogEntity.setRevise(reviseEntity);
        return reviseLogEntity;
    }
}
