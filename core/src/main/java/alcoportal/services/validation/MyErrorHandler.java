package alcoportal.services.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;


import javax.xml.parsers.SAXParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by vladimir akummail@gmail.com on 10/28/15.
 */
public class MyErrorHandler extends DefaultHandler implements ErrorHandler {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    List<ErrorItem> errorItems;
    javax.xml.validation.Validator validator;

    public MyErrorHandler(javax.xml.validation.Validator validator) {
        this.errorItems = new ArrayList<>();
        this.validator = validator;
    }

    public List<ErrorItem> getErrorItems() {
        return errorItems;
    }

    public void warning(SAXParseException e) throws SAXException {
        makeError(e);
    }

    public void error(SAXParseException e) throws SAXException {
        makeError(e);
    }

    public void fatalError(SAXParseException e) throws SAXException {
        makeError(e);
    }

    private void makeError(SAXParseException e) throws SAXException {
          /*  System.out.println("   Public ID: " + e.getPublicId());
            System.out.println("   System ID: " + e.getSystemId());
            System.out.println("   Line number: " + e.getLineNumber());
            System.out.println("   Column number: " + e.getColumnNumber());
            System.out.println("   Message: " + e.getMessage());*/
        ///  elementName = currentElement();
       // log.warn("Ошибка валидации \n" + e.getMessage());
        ErrorItem errorItem = new ErrorItem()
                .setLineNumber(e.getLineNumber()).setDescription(e.getMessage());
        try {
            Element invalidElement = (Element) validator.getProperty("http://apache.org/xml/properties/dom/current-element-node");
            String elementName = invalidElement.getTagName();
            //   System.out.println("Error: " + e.getMessage());
            //  System.out.println("   Invalid element: " + invalidElement.getTagName());


            //  System.out.println("   Value          : " + invalidElement.getTextContent());
            errorItem.setElementName(elementName);
        } catch (Exception ex) {
            errorItem.setElementName(null);
        }

        errorItems.add(errorItem);
    }
}
