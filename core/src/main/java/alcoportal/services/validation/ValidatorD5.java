package alcoportal.services.validation;

import alcoportal.exseptions.ValidateExseption;

import java.io.File;
import java.io.InputStream;

/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
public class ValidatorD5 extends ValidateXml implements Validator {
    static final String NAME = "D5";
    static final String FILE_XSD_NAME = "XSD/D5.xsd";

    @Override
    public void validate(InputStream inputStream) throws ValidateExseption {
        validate(inputStream, FILE_XSD_NAME);
    }
}
