package alcoportal.services.validation;

import alcoportal.exseptions.ValidateExseption;
import java.io.File;
import java.io.InputStream;


/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
public interface Validator {
    void validate(InputStream inputStream) throws ValidateExseption;
}
