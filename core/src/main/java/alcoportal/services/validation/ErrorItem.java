package alcoportal.services.validation;

/**
 * Created by vladimir akummail@gmail.com on 10/28/15.
 */
public class ErrorItem implements Comparable<ErrorItem> {
    int lineNumber;
    String elementName;
    String description;

    public String getDescription() {
        return description;
    }

    public ErrorItem setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getElementName() {
        return elementName;
    }

    public ErrorItem setElementName(String elementName) {
        this.elementName = elementName;
        return this;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public ErrorItem setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ErrorItem errorItem = (ErrorItem) o;

        if (lineNumber != errorItem.lineNumber) return false;
        return !(elementName != null ? !elementName.equals(errorItem.elementName) : errorItem.elementName != null);

    }

    @Override
    public int hashCode() {
        int result = lineNumber;
        result = 31 * result + (elementName != null ? elementName.hashCode() : 0);
        return result;
    }


    @Override
    public int compareTo(ErrorItem errorItem) {
        if (errorItem.getLineNumber() < this.getLineNumber()) {
            return 1;
        } else if (errorItem.getLineNumber() > this.getLineNumber()) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "ErrorItem{" +
                "lineNumber=" + lineNumber +
                ", elementName='" + elementName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
