package alcoportal.services.validation;

import alcoportal.dto.FileNameDTO;
import alcoportal.exseptions.ValidateExseption;
import org.springframework.stereotype.Service;

/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
@Service
public class ValidatorFactory {
    public Validator get(FileNameDTO fileNameDTO) throws ValidateExseption {
        switch (fileNameDTO.getFormName()) {
            case ValidatorD5.NAME:
                return new ValidatorD5();
            case ValidatorD6.NAME:
                return new ValidatorD6();
            case ValidatorD7.NAME:
                return new ValidatorD7();
            default:
                throw new ValidateExseption("Для документа не найдена схема валидации.");
        }
    }
}
