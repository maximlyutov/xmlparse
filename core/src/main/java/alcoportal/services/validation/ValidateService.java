package alcoportal.services.validation;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.ValidationErrorEntity;
import alcoportal.dal.domain.entity.revise.ReviseEntity;
import alcoportal.dal.repository.DocumentHistoryRepository;
import alcoportal.dal.repository.ReviseRepository;
import alcoportal.dal.repository.ValidationErrorRepository;
import alcoportal.dto.DocumentHistoryDTO;
import alcoportal.dto.FileNameDTO;
import alcoportal.exseptions.ParsingExseption;
import alcoportal.exseptions.ValidateExseption;
import alcoportal.services.parsing.ParsingService;
import alcoportal.utils.FormNameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
@Service
public class ValidateService {
    public static final int ERROR_COUNT = 20;
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ValidatorFactory validatorFactory;

    @Autowired
    private DocumentHistoryRepository documentHistoryRepository;

    @Autowired
    ValidationErrorRepository validationErrorRepository;

    @Autowired
    ParsingService parsingService;

    @Autowired
    ReviseRepository reviseRepository;

    public DocumentHistoryDTO validateAndSave(String fileName, byte[] bytes, UserEntity userEntity) {
        DocumentHistoryEntity documentHistoryEntity = new DocumentHistoryEntity();
        documentHistoryEntity.setUser(userEntity);
        FileNameDTO fileNameDTO;

        try {
            documentHistoryEntity.setCreateAt(new Date());
            documentHistoryEntity.setFileName(fileName);
            documentHistoryEntity.setDocument(bytes);
            fileNameDTO = FormNameUtils.GetNameDTOFromString(fileName);
            documentHistoryEntity.setFormName(fileNameDTO.getFormName());
            SimpleDateFormat df = new SimpleDateFormat("yyyy");
            documentHistoryEntity.setYear(df.format(fileNameDTO.getDate())); //todo надо брать из документа
            documentHistoryEntity.setQuarter(fileNameDTO.getQuartal());
        } catch (Exception e) {
            e.printStackTrace();
            documentHistoryEntity.setValidateState(false);
            documentHistoryEntity.setDescription(e.getMessage());
            documentHistoryRepository.save(documentHistoryEntity);
            return documentHistoryEntityToDTO(documentHistoryEntity);
        }

        try {
            InputStream inputStream = new ByteArrayInputStream(bytes);
            Validator validator = validatorFactory.get(fileNameDTO);
            validator.validate(inputStream);
            validateInn(bytes, userEntity);
            documentHistoryEntity.setValidateState(true);
        } catch (ValidateExseption e) {
            log.info(e.getMessage());
            documentHistoryEntity.setValidateState(false)
                    .setDescription(localizationError(e.getErrorItems(), fileNameDTO.getFormName(), e.getMessage()));
        } catch (Exception e) {
            log.info(e.getMessage());
            documentHistoryEntity.setValidateState(false)
                    .setDescription("Ошибка валидации файла");
        }
        documentHistoryRepository.save(documentHistoryEntity);

        if (documentHistoryEntity.isValidateState()) {
            try {
                parsingService.parsing(documentHistoryEntity);
            } catch (ParsingExseption parsingExseption) {
                documentHistoryEntity.setValidateState(false)
                        .setDescription(parsingExseption.getMessage());
            }

        }

        return documentHistoryEntityToDTO(documentHistoryEntity);

    }

    public List<DocumentHistoryDTO> getDocumentsHistory(UserEntity userEntity) {
        List<DocumentHistoryDTO> documentHistoryDTOs = new ArrayList<>();

        documentHistoryRepository.findAllByUserOrderByCreateAtDesc(userEntity).forEach(documentHistory -> {
            DocumentHistoryDTO dto = documentHistoryEntityToDTO(documentHistory);
            ReviseEntity reviseEntity = reviseRepository.findOneByDocumentHistory(documentHistory);
            if (reviseEntity != null) {
                dto.setReviseState(reviseEntity.getReviseState());
            }

            documentHistoryDTOs.add(dto);
        });
        return documentHistoryDTOs;
    }


    public DocumentHistoryEntity getDocumentHistory(UserEntity userEntity, long documentHistoryId) {
        return documentHistoryRepository.findOneByUserAndId(userEntity, documentHistoryId);
    }


    private DocumentHistoryDTO documentHistoryEntityToDTO(DocumentHistoryEntity documentHistoryEntity) {
        String quarter = null;
        if (documentHistoryEntity.getQuarter() != null) {
            quarter = String.valueOf(Long.parseLong(documentHistoryEntity.getQuarter()) / 3);
        }
        return new DocumentHistoryDTO()
                .setId(documentHistoryEntity.getId())
                .setFileName(documentHistoryEntity.getFileName())
                .setValidateState(documentHistoryEntity.isValidateState())
                .setType(documentHistoryEntity.getFormName())
                .setCreateAt(documentHistoryEntity.getCreateAt())
                .setDescription(documentHistoryEntity.getDescription())
                .setQuarter(quarter)
                .setYear(documentHistoryEntity.getYear())
                .setFullReviseState(documentHistoryEntity.isFullReviseState())
                .setAlreadyRevised(documentHistoryEntity.getAlreadyRevised());
    }

    private String localizationError(List<ErrorItem> errorItems, String formName, String msgError) {
        if (formName == null || (errorItems == null || errorItems.isEmpty())) {
            return msgError;
        }

        StringBuilder msg = new StringBuilder("Ошибка валидации");
        Set<ErrorItem> uniqueErrors = new HashSet<>(errorItems);

        List<ErrorItem> sortedErrors = new ArrayList<>(uniqueErrors);
        Collections.sort(sortedErrors);

        for (int i = 0; i < sortedErrors.size() && i < ERROR_COUNT; i++) {
            ErrorItem elementItem = sortedErrors.get(i);
            if (elementItem.getElementName() == null) {
                continue;
            }

            ValidationErrorEntity validationErrorEntity = validationErrorRepository.findByFormNameAndElementName(formName, elementItem.getElementName());
            if (validationErrorEntity == null) {
                validationErrorEntity = new ValidationErrorEntity()
                        .setElementName(elementItem.getElementName())
                        .setFormName(formName)
                        .setDescription(elementItem.getElementName());
                validationErrorRepository.save(validationErrorEntity);
            }
            msg
                    .append("<br />")
                    .append("<br />")
                    .append("элемент : '").append(validationErrorEntity.getDescription())
                    .append("' строка : ").append(elementItem.getLineNumber());
            //.append("<br />")
            //.append(elementItem.getDescription())
            //.append("<br />")
            //.append("<br />");
        }

        return msg.toString();
    }

    private void validateInn(byte[] bytes, UserEntity userEntity) throws ValidateExseption {
        try {
            InputStream file = new ByteArrayInputStream(bytes);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();

            NodeList turnovers = doc.getElementsByTagName("Реквизиты");
            Element element = (Element) turnovers.item(0);
            NodeList ulNode = element.getElementsByTagName("ЮЛ");
            Element elementUl = (Element) ulNode.item(0);
            log.info("ИНН документа " + elementUl.getAttribute("ИННЮЛ"));
            if (userEntity.getInn() != Long.parseLong(elementUl.getAttribute("ИННЮЛ"))) {
                throw new ValidateExseption("Не совпадает ИНН");
            }

        } catch (Exception e) {
            throw new ValidateExseption("Не совпадает ИНН");
        }
    }
}
