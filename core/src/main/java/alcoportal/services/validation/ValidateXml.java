package alcoportal.services.validation;

import alcoportal.exseptions.ValidateExseption;
import alcoportal.exseptions.ValidateSAXException;
import alcoportal.utils.ResourseFileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vladimir akummail@gmail.com on 10/4/15.
 */
@Service
public class ValidateXml {
    protected javax.xml.validation.Validator validator;
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    public void validate(InputStream inputStream, String xsdFileName) throws ValidateExseption {

        try {
            StreamSource xsdFile;
            try {
                xsdFile = ResourseFileUtils.getFileFromResource(xsdFileName, getClass().getClassLoader());
            } catch (Exception e) {
                throw new ValidateExseption("Для документа не найдена схема валидации.", null);
            }
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder parser = documentBuilderFactory.newDocumentBuilder();

            try {
                org.w3c.dom.Document document = parser.parse(inputStream);
                SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

                Schema schema = factory.newSchema(xsdFile);
                validator = schema.newValidator();
                MyErrorHandler myErrorHandler = new MyErrorHandler(validator);

                validator.setErrorHandler(myErrorHandler);
                validator.validate(new DOMSource(document));

                setLineNumber(xsdFileName, inputStream, myErrorHandler.getErrorItems());
                if (!myErrorHandler.getErrorItems().isEmpty()) {

                    for (ErrorItem item: myErrorHandler.getErrorItems()){
                        log.info(item.toString());
                    }
                    throw new ValidateExseption("Ошибка валидации", myErrorHandler.getErrorItems());
                }
            } catch (ValidateExseption e) {
                throw e;
            } catch (Exception e) {
                throw new ValidateExseption("Ошибка валидации.");
            }
        } catch (ParserConfigurationException e) {
            throw new ValidateExseption("Ошибка валидации.");
        }
    }

    void setLineNumber(String xsdFileName, InputStream inputStream, List<ErrorItem> errorItems) throws SAXException, ParserConfigurationException, IOException, NoSuchFieldException {
        inputStream.reset();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        factory.setSchema(schemaFactory.newSchema(ResourseFileUtils.getFileFromResource(xsdFileName, getClass().getClassLoader())));
        factory.setNamespaceAware(true);
        SAXParser parser = factory.newSAXParser();

        MyErrorHandlerLiner myErrorHandler = new MyErrorHandlerLiner(errorItems);
        parser.parse(inputStream, myErrorHandler);
    }


    public class MyErrorHandlerLiner extends DefaultHandler {
        private Locator locator;
        List<ErrorItem> errorItems;
        int current = 0;

        public MyErrorHandlerLiner(List<ErrorItem> errorItems) {
            this.errorItems = errorItems;
        }

        public void setDocumentLocator(Locator locator) {
            this.locator = locator;
        }

        public void warning(SAXParseException e) throws SAXException {
            makeError(e);
        }

        public void error(SAXParseException e) throws SAXException {
            makeError(e);
        }

        public void fatalError(SAXParseException e) throws SAXException {
            makeError(e);
        }

        private void makeError(SAXParseException e) throws SAXException {
            ErrorItem errorItem = errorItems.get(current);
            errorItem.setLineNumber(e.getLineNumber());
            current++;
        }

    }

}
