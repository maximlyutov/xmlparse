import alcoportal.exseptions.ValidateExseption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.xml.sax.SAXException;
import alcoportal.services.validation.ValidateXml;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Arrays;


@RunWith(value = Parameterized.class)
public class XmlValidate {

    private String xml;
    private String xsd;

    public XmlValidate(String xml, String xsd) {
        this.xml = xml;
        this.xsd = xsd;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"XML/ur1/D5_7715910836_035_21050916_00505696-76C4-1EE5-96FF-CBCD3DFB9684.xml", "XSD/5-o.xsd"},
                {"XML/ur1/D5_7715910836_065_21050916_00505696-76C4-1EE5-96FF-D840E21DD684.xml", "XSD/5-o.xsd"},
                {"XML/ur1/D6_7715910836_035_21050916_00505696-76C4-1EE5-96FF-E3ABF6B59684.xml", "XSD/6-o.xsd"},
                {"XML/ur1/D6_7715910836_065_21050916_00505696-76C4-1EE5-9780-11E396379684.xml", "XSD/6-o.xsd"},
                {"XML/ur1/D7_7715910836_035_21050916_00505696-76C4-1EE5-9780-31ADF8F99684.xml", "XSD/7-o.xsd"},
                {"XML/ur1/D7_7715910836_065_21050916_00505696-76C4-1EE5-9780-3C71715D9684.xml", "XSD/7-o.xsd"},
                {"XML/ur2/D5_7716743810_035_21050916_00505696-76C4-1EE5-96FF-D12A43259684.xml", "XSD/5-o.xsd"},
                {"XML/ur2/D5_7716743810_065_21050916_00505696-76C4-1EE5-96FF-FC06B669D684.xml", "XSD/5-o.xsd"},
                {"XML/ur2/D6_7716743810_035_21050916_00505696-76C4-1EE5-9780-2347F1ED9684.xml", "XSD/6-o.xsd"},
                {"XML/ur2/D6_7716743810_065_21050916_00505696-76C4-1EE5-96FF-DF62D191D684.xml", "XSD/6-o.xsd"},
                {"XML/ur2/D7_7716743810_03_20150916_00505696-76C4-1EE5-9780-3F1A5401D684.xml", "XSD/7-o.xsd"},
                {"XML/ur2/D7_7716743810_065_21050916_00505696-76C4-1EE5-9780-41C2F055D684.xml", "XSD/7-o.xsd"},
                {"XML/ur3/D5_3444136778_035_21050916_00505696-76C4-1EE5-96FF-D5C295D19684.xml", "XSD/5-o.xsd"},
                {"XML/ur3/D5_7736657630_065_21050916_00505696-76C4-1EE5-9780-1FCBCF35D684.xml", "XSD/5-o.xsd"},
                {"XML/ur3/D6_7736657630_035_21050916_00505696-76C4-1EE5-9780-10D0536B9684.xml", "XSD/6-o.xsd"},
                {"XML/ur3/D6_7736657630_065_21050916_00505696-76C4-1EE5-96FF-E6C1ED2D9684.xml", "XSD/6-o.xsd"},
                {"XML/ur3/D7_7718929024_035_21050916_00505696-76C4-1EE5-9780-47528DA59684.xml", "XSD/7-o.xsd"},
                {"XML/ur3/D7_7718929024_065_21050916_00505696-76C4-1EE5-9780-4557C5519684.xml", "XSD/7-o.xsd"}
        });
    }

    @Test
    public void validate() throws NoSuchFieldException, IOException, SAXException, ParserConfigurationException, ValidateExseption {
        ValidateXml validateXml = new ValidateXml();
        validateXml.validate(ClassLoader.getSystemClassLoader().getResourceAsStream(xml), xsd);
    }
}
