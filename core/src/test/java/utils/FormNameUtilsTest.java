package utils;

import alcoportal.dto.FileNameDTO;
import alcoportal.exseptions.ValidateExseption;
import alcoportal.utils.FormNameUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormNameUtilsTest {
    @Test
    public void testGetNameDTOFromString() throws Exception {

        DateFormat format = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH);
        Date date = format.parse("21052015");

        FileNameDTO fileNameDTOexpected = new FileNameDTO()
                .setFormName("D5")
                .setIin("7715910836")
                .setQuartal("03")
                .setDate(date)
                .setId("00505696-76C4-1EE5-96FF-CBCD3DFB9684");

        FileNameDTO actualFileNameDTO = FormNameUtils.GetNameDTOFromString("D5_7715910836_03_21052015_00505696-76C4-1EE5-96FF-CBCD3DFB9684.xml");
        Assert.assertEquals(fileNameDTOexpected, actualFileNameDTO);
    }

    @Test(expected = ValidateExseption.class)
    public void getNameDTOFromStringNotValid() throws ValidateExseption, ParseException, UnsupportedEncodingException {
        FormNameUtils.GetNameDTOFromString("D5_7715910836_035_21050916_00505696-76C4-1EE5-96FF-CBCD3DFB9684.xml");
    }
}