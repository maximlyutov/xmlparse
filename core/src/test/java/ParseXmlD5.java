import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import alcoportal.utils.ResourseFileUtils;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.InputStream;


public class ParseXmlD5 {
    String fileXml = "XML/ur1/D5_7715910836_035_21050916_00505696-76C4-1EE5-96FF-CBCD3DFB9684.xml";

    @Test
    public void parseD5() {
        try {

            InputStream file = ClassLoader.getSystemClassLoader().getResourceAsStream(fileXml);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("ОбъемОборота");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    boolean turnover = Boolean.parseBoolean(eElement.getAttribute("НаличиеОборота"));
                    System.out.println("НаличиеОборота : " + turnover);
                    if (turnover) {
                        NodeList iNodes = ((Element) nNode).getElementsByTagName("Оборот");
                        //TODO Получаем из элемента оборот порядковый номер и тип
                        for (int i = 0; i < iNodes.getLength(); i++) {
                            Node currentNode = iNodes.item(i);
                            System.out.println("\nКод вида продукции  :" + ((Element) currentNode).getAttribute("П000000000003"));
                            NodeList providerNodes = ((Element) currentNode).getElementsByTagName("СведПроизвИмпорт");
                            for (int y = 0; y < providerNodes.getLength(); y++) {
                                Node providerNode = providerNodes.item(y);

                                System.out.println("\nОрганизация :" + ((Element) providerNode).getAttribute("П000000000004"));
                                ////TODO Вытаскиваем все поля из производителя
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
