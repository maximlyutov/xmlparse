define([
    'jquery',
    'underscore',
    'uploadView',
    'uploadModel',
    'declarationsCollection'
], function ($, _, UploadView, UploadModel, Declarations) {
    'use strict';

    _.templateSettings = {
        interpolate: /\{\{(.+?)\}\}/g
    };

    return {
        init: function () {
            function startFullRevice(self) {
                var row = $(self.toElement).parent().parent();
                var postfix = row.find('input[name=id]').val();

                var revice = row.find('input[name=revice]').prop("checked");
                var choosedRevice = row.find('input[name=choosedRevice]').prop("checked");

                var url = "/revise/build/" + postfix;

                $.post(url, {revice: revice, choosedRevice: choosedRevice},
                    function () {
                        location.reload();
                    })
                    .fail(function () {
                        location.reload();
                    });

                //  console.log(self);
            }
            function deleteRevice(self) {
                var row = $(self.toElement).parent().parent();
                var postfix = row.find('input[name=id]').val();

                var revice = row.find('input[name=revice]').prop("checked");
                var choosedRevice = row.find('input[name=choosedRevice]').prop("checked");

                var url = "/revise/delete/" + postfix;

                $.post(url,
                    function () {
                        location.reload();
                    })
                    .fail(function () {
                        location.reload();
                    });

                //  console.log(self);
            }



            $("button[name='buttonrow']").on('click', startFullRevice);
            $("button[name='revise_delete']").on('click', deleteRevice);



            var $uploadWrapper = $('.upload-wrapper'),
                declarations = new Declarations(),
                uploadModel = new UploadModel({declarations: declarations});

            if ($uploadWrapper.length > 0) {
                new UploadView({
                    el: $uploadWrapper,
                    model: uploadModel
                }).render();
            }

            var modalActive = function () {
                $('#orderModal').modal({
                    keyboard: true,
                    backdrop: "static",
                    show: false
                }).on('show.bs.modal', function (e) {
                    var getErrorFromRow = $(event.target).closest('tr').find("#analyze").find("div").html();
                    //make your ajax call populate items or what even you need
                    $(this).find('#orderDetails').html($('<b>' + getErrorFromRow + '</b>'))
                })
            };

            declarations.on('add', function (model, collection, opts) {
                var $blankRow = $('#tableFormBody').find("tr.blank").clone();
                $blankRow.removeClass('blank');

                if (model.attributes.status) {
                    $blankRow.addClass("success");
                } else {
                    $blankRow.find("input[name='revice']").attr("disabled", true);
                    $blankRow.find("input[name='choosedRevice']").attr("disabled", true);
                    $blankRow.find("button[name='buttonrow']").attr("disabled", true);
                }
                var tmpRow = _.template($blankRow[0].outerHTML),
                    dataRow = tmpRow(model.attributes);
                $('#tableFormBody').prepend(dataRow);
                $("button[name='buttonrow']").on('click', startFullRevice);
                $("button[name='revise_delete']").on('click', deleteRevice);
                modalActive();

                return;
            });

            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-tabs a[href=#' + url.split('#')[1] + ']').tab('show');
            }


            modalActive();

            var inputSelect = 'input[type="text"], input[type="email"], textarea',
                $inputs = $('.office-forms').find(inputSelect);
            if ($inputs.length > 0) {
                $inputs.prop('disabled', true);
                $('.office-forms .form-group').bind('click', function () {
                    $(this).find(inputSelect).prop('disabled', false).focus();
                    return true;
                });
            }

            // Change hash for page-reload
            $('.nav-tabs a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            });


            return;
        }
    };
});