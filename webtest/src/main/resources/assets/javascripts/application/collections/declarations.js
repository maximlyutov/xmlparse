define([
  'jquery',
  'underscore',
  'backbone',
  'declarationModel'
], function ($, _, Backbone, Declaration) {
  'use strict';

  var Declarations = Backbone.Collection.extend({
    model: Declaration
  });

  return Declarations;
});