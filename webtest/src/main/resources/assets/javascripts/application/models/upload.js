define([
  'jquery',
  'underscore',
  'backbone',
  'declarationsCollection'
], function ($, _, Backbone, Declarations) {
  'use strict';

  var UploadModel = Backbone.Model.extend({
    defaults: function() {
      return {
        state: 'ready',
        declarations: new Declarations()
      }
    }
  });

  return UploadModel;
});