define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {
  'use strict';

  var DeclarationsModel = Backbone.Model.extend({
    defaults: function() {
      return {
        id: '',
        form: 'DX',
        year: '',
        quarter: '',
        date: '1970-01-01 00:00:00',
        status: '-',
        analyze: '-',
        reviseStatus: ''
      }
    }
  });

  return DeclarationsModel;
});