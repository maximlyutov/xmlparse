define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {
  'use strict';

  var UploadView = Backbone.View.extend({
    events: {
      'submit form': 'onSubmit'
    },
    initialize: function() {
      this.$formWrapper = this.$el.find('.form-wrapper');
      this.$loadWrapper = this.$el.find('.load-wrapper');
      this.$uploadForm = this.$el.find('form');
      this.$fileInput = this.$uploadForm.find('[type="file"]');

      return;
    },
    onSubmit: function(event) {
      event.preventDefault();

      var self = this, 
        formData = new FormData(this.$uploadForm[0]);

      if (this.$fileInput.val() == "") {
        alert('Выберите файл');
        return;
      }

      $.ajax({
        url: this.$uploadForm.attr('action'),
        type: 'POST',
        dataType: 'json',
        data: formData,
        success: this.uploadComplete,
        error: this.uploadError,
        cache: false,
        contentType: false,
        processData: false,
        context: this
      }).always(function() {
        self.model.set('state', 'ready');
        return this.render();
      });

      this.model.set('state', 'loading');
      this.render();

      return;
    },
    uploadComplete: function(data, status, xhr) {
      this.model.get('declarations').add(data);
      return;
    },
    uploadError: function(xhr, status, msg) {
      return this.render();
    },
    render: function() {
      if (this.model.get('state') == 'ready') {
        this.$fileInput.val(null)
        this.$formWrapper.show();
        this.$loadWrapper.hide();
      } else {
        this.$formWrapper.hide();
        this.$loadWrapper.show();
      }
      return this;
    }
  });

  return UploadView;
});