define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {
  'use strict';

  var TableView = Backbone.View.extend({
    events: {
      'submit form': 'onSubmit'
    },
    initialize: function() {

    },
    onSubmit: function() {

    },
    render: function() {
      return this;
    }
  });

  return TableView
});
