'use strict';

requirejs.config({
  waitSeconds: 300,
  paths: {
    text: '../vendor/requirejs-text/text',
    domReady: '../vendor/requirejs-domready/domReady',
    jquery: '../vendor/jquery/dist/jquery',
    underscore: '../vendor/underscore/underscore',
    bootstrap: '../vendor/bootstrap/dist/js/bootstrap',
    backbone: '../vendor/backbone/backbone',
    
    declarationsCollection: 'application/collections/declarations',
    declarationModel: 'application/models/declaration',
    uploadModel: 'application/models/upload',
    uploadView: 'application/views/upload',
    application: 'application'
  },
  shim: {
    underscore: { exports: '_' },
    backbone: {
      deps: ['underscore', 'jquery'], exports: 'Backbone'
    },
    bootstrap : { deps :['jquery'] }
  }
});

require([
  'domReady!',
  'application',
  'bootstrap'
], function (ready, Application) {
  Application.init();
  return;
});
