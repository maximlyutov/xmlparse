package alcoportal.dto;


import alcoportal.dal.domain.other.OrganisationTypeEnum;
import alcoportal.validation.ValidEmail;
import alcoportal.validation.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull
    private Long inn;

    @ValidPassword
    private String password;

    @ValidEmail
    @NotNull
    @Size(min = 1)
    private String email;

    @NotNull
    @Size(min = 9, max = 9)
    private Long kpp;

    @NotNull
    private String orgName;

    @NotNull
    private String phoneNumber;

    @NotNull
    private OrganisationTypeEnum orgType;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public OrganisationTypeEnum getOrgType() {
        return orgType;
    }

    public void setOrgType(OrganisationTypeEnum orgType) {
        this.orgType = orgType;
    }

    public Long getKpp() {
        return kpp;
    }

    public void setKpp(Long kpp) {
        this.kpp = kpp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Long getInn() {
        return inn;
    }

    public void setInn(Long inn) {
        this.inn = inn;
    }
}
