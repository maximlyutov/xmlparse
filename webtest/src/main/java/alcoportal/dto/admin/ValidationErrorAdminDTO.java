package alcoportal.dto.admin;

/**
 * Created by argutin on 02.11.15.
 */
public class ValidationErrorAdminDTO {
    private String description;

    public String getDescription() {
        return description;
    }

    public ValidationErrorAdminDTO setDescription(String description) {
        this.description = description;
        return this;
    }
}
