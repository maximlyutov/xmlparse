package alcoportal.dto.userInfo;

/**
 * Created by good on 31.10.2015.
 */
public class UserInfoOrganisationDTO {

    private String orgName;
    private Long orgInn;
    private Long orgKpp;
    private String fioDir;
    private String fioBuh;
    private String subjectCode;
    private String orgAddress;
    private String orgEmail;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getOrgInn() {
        return orgInn;
    }

    public void setOrgInn(Long orgInn) {
        this.orgInn = orgInn;
    }

    public Long getOrgKpp() {
        return orgKpp;
    }

    public void setOrgKpp(Long orgKpp) {
        this.orgKpp = orgKpp;
    }

    public String getFioDir() {
        return fioDir;
    }

    public void setFioDir(String fioDir) {
        this.fioDir = fioDir;
    }

    public String getFioBuh() {
        return fioBuh;
    }

    public void setFioBuh(String fioBuh) {
        this.fioBuh = fioBuh;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getOrgAddress() {
        return orgAddress;
    }

    public void setOrgAddress(String orgAddress) {
        this.orgAddress = orgAddress;
    }

    public String getOrgEmail() {
        return orgEmail;
    }

    public void setOrgEmail(String orgEmail) {
        this.orgEmail = orgEmail;
    }
}
