package alcoportal.dto.userInfo;

/**
 * Created by good on 04.11.2015.
 */
public class UserInfoSubdivisionDTO {
    private String sdKpp;
    private String address;
    private String subjectCode;

    public String getSdKpp() {
        return sdKpp;
    }

    public void setSdKpp(String sdKpp) {
        this.sdKpp = sdKpp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }
}
