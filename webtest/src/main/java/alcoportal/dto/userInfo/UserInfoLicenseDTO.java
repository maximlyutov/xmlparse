package alcoportal.dto.userInfo;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.userInfo.SubdivisionEntity;

import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Created by good on 04.11.2015.
 */
public class UserInfoLicenseDTO {

    private Long subdivisionId;
    private String workType;
    private String dateStart;
    private String dateEnd;
    private String number;
    private String numberBlank;

    public Long getSubdivisionId() {
        return subdivisionId;
    }

    public void setSubdivisionId(Long subdivisionId) {
        this.subdivisionId = subdivisionId;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumberBlank() {
        return numberBlank;
    }

    public void setNumberBlank(String numberBlank) {
        this.numberBlank = numberBlank;
    }
}
