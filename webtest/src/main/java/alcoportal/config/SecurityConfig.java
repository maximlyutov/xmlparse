package alcoportal.config;

import alcoportal.security.CustomRememberMeTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;

/**
 * Created by good on 14.10.2015.
 */
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static PasswordEncoder encoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private CustomRememberMeTokenRepository customRememberMeTokenRepository;


//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.
//                authenticationProvider(authProvider())
//                .formLogin().loginPage("/login").failureUrl("/login-error")
//                .and().logout()
//                .and().authorizeRequests().antMatchers("/admin/**").hasRole("ADMIN")
//
//        ;
//    }

    @Bean
    public DefaultWebSecurityExpressionHandler webSecurityExpressionHandler() {
        return new DefaultWebSecurityExpressionHandler();
    }


    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/assets/**");
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/j_spring_security_check*", "/login*", "/logout*", "/signin/**", "/signup/**",
                        "/registration", "/expiredAccount*",
                        "/badUser*", "/user/resendRegistrationToken*", "/forgetPassword*", "/user/resetPassword*",
                        "/user/changePassword*", "/emailError*", "/resources/**", "/assets/**", "/successRegister*").anonymous()  //,"/revise/build/*"
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/index", "/userinfo","/documenthistoru/*").hasAnyRole("USER", "ADMIN")
                .antMatchers("/registrationConfirm").permitAll()
                .and()
                .formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/j_spring_security_check")
                .defaultSuccessUrl("/index.html")
                .failureUrl("/login.html?error=true")
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .permitAll()
                .and()
                .sessionManagement()
                .invalidSessionUrl("/login.html")
                .sessionFixation().none()
                .and()
                .rememberMe()
                .userDetailsService(userDetailsService)
                .rememberMeParameter("_spring_security_remember_me")
                .tokenRepository(customRememberMeTokenRepository)
                .tokenValiditySeconds(1209600)
                .and()
                .logout()
                .invalidateHttpSession(true)
                .logoutUrl("/j_spring_security_logout")
                .logoutSuccessUrl("/index.html")
                .deleteCookies("JSESSIONID")
                .permitAll()
        ;
        // @formatter:on
    }

    @Bean
    @DependsOn("passwordEncoder")
    public DaoAuthenticationProvider authProvider() {
        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder);
        return authProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        if (encoder == null) {
            encoder = new BCryptPasswordEncoder();
        }
        return encoder;
    }
}
