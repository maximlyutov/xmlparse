package alcoportal.service;

import alcoportal.dal.domain.entity.PasswordResetTokenEntity;
import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.VerificationTokenEntity;
import alcoportal.dto.UserDto;
import alcoportal.error.EmailExistsException;

public interface IUserService {

    UserEntity registerNewUserAccount(UserDto accountDto) throws EmailExistsException;

    void saveRegisteredUser(UserEntity user);

    void createVerificationTokenForUser(UserEntity user, String token);

    VerificationTokenEntity getVerificationToken(String VerificationToken);

    void createPasswordResetTokenForUser(UserEntity user, String token);

    PasswordResetTokenEntity getPasswordResetToken(String token);

    UserEntity getUserByPasswordResetToken(String token);

    void changeUserPassword(UserEntity user, String password);

    boolean checkIfValidOldPassword(UserEntity user, String password);

}
