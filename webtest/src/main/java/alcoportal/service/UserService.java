package alcoportal.service;

import alcoportal.dal.domain.entity.PasswordResetTokenEntity;
import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.VerificationTokenEntity;
import alcoportal.dal.domain.other.RoleEnum;
import alcoportal.dal.repository.PasswordResetTokenRepository;
import alcoportal.dal.repository.UserEntityRepository;
import alcoportal.dal.repository.UserRoleEntityRepository;
import alcoportal.dal.repository.VerificationTokenEntityRepository;
import alcoportal.dto.UserDto;
import alcoportal.dto.userInfo.UserInfoOrganisationDTO;
import alcoportal.error.EmailExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;

@Service
@Transactional
public class UserService implements IUserService {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private VerificationTokenEntityRepository verificationTokenEntityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRoleEntityRepository userRoleEntityRepository;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private PasswordResetTokenRepository passwordResetTokenRepository;

    public UserEntity getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        return userEntityRepository.findByEmail(name);
    }

    @Override
    @Transactional
    public UserEntity registerNewUserAccount(@Valid final UserDto accountDto) throws EmailExistsException {
        if (emailExist(accountDto.getEmail())) {
            throw new EmailExistsException("There is an account with that email address: " + accountDto.getEmail());
        }

        final UserEntity user = new UserEntity();

        user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        user.setEmail(accountDto.getEmail());
        user.setPhoneNumber(accountDto.getPhoneNumber());
        user.getRoles().add(userRoleEntityRepository.findByTitle(RoleEnum.ROLE_USER));
        user.setInn(accountDto.getInn());
        userEntityRepository.save(user);

        UserInfoOrganisationDTO userInfoOrganisationDTO = new UserInfoOrganisationDTO();
        userInfoOrganisationDTO.setOrgKpp(accountDto.getKpp());
        userInfoOrganisationDTO.setOrgName(accountDto.getOrgName());
        if (accountDto.getOrgType() != null) {
            userInfoOrganisationDTO.setOrgName(accountDto.getOrgName());
        }

        userInfoService.saveOrganisation(userInfoOrganisationDTO, user.getId());

        return user;
    }

    @Override
    public VerificationTokenEntity getVerificationToken(final String VerificationToken) {
        return verificationTokenEntityRepository.findByToken(VerificationToken);
    }

    @Override
    public void saveRegisteredUser(final UserEntity user) {
        userEntityRepository.save(user);
    }

    @Override
    public void createVerificationTokenForUser(final UserEntity user, final String token) {
        final VerificationTokenEntity myToken = new VerificationTokenEntity(token, user);
        verificationTokenEntityRepository.save(myToken);
    }

//    @Override
//    public VerificationToken generateNewVerificationToken(final String existingVerificationToken) {
//        VerificationToken vToken = tokenRepository.findByToken(existingVerificationToken);
//        vToken.updateToken(UUID.randomUUID().toString());
//        vToken = tokenRepository.save(vToken);
//        return vToken;
//    }

    @Override
    public void createPasswordResetTokenForUser(final UserEntity user, final String token) {
        final PasswordResetTokenEntity myToken = new PasswordResetTokenEntity(token, user);
        passwordResetTokenRepository.save(myToken);
    }
    @Override
    public PasswordResetTokenEntity getPasswordResetToken(final String token) {
        return passwordResetTokenRepository.findByToken(token);
    }
    @Override
    public UserEntity getUserByPasswordResetToken(final String token) {
        return passwordResetTokenRepository.findByToken(token).getUser();
    }

    @Transactional
    @Override
    public void changeUserPassword(final UserEntity user, final String password) {
        user.setPassword(passwordEncoder.encode(password));
        userEntityRepository.save(user);
    }

    @Override
    public boolean checkIfValidOldPassword(final UserEntity user, final String oldPassword) {
        return passwordEncoder.matches(oldPassword, user.getPassword());
    }
    private boolean emailExist(final String email) {
        final UserEntity user = userEntityRepository.findByEmail(email);
        return user != null;
    }
}