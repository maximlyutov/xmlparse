package alcoportal.service;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.userInfo.LicenseEntity;
import alcoportal.dal.domain.entity.userInfo.OrganisationEntity;
import alcoportal.dal.domain.entity.userInfo.SubdivisionEntity;
import alcoportal.dal.repository.LicenseEntityRepository;
import alcoportal.dal.repository.OrganisationEntityRepository;
import alcoportal.dal.repository.SubdivisionEntityRepository;
import alcoportal.dal.repository.UserEntityRepository;
import alcoportal.dto.userInfo.UserInfoContactDTO;
import alcoportal.dto.userInfo.UserInfoOrganisationDTO;
import alcoportal.registration.OnRegistrationCompleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by argutin on 02.11.15.
 */
@Service
public class UserInfoService {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private OrganisationEntityRepository organisationEntityRepository;

    @Autowired
    private LicenseEntityRepository licenseEntityRepository;

    @Autowired
    private SubdivisionEntityRepository subdivisionEntityRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    public ModelAndView getModelForUserInfo(ModelAndView model, Long id) {
        UserEntity user;
        if (id == null) {
            user = userService.getCurrentUser();
        } else {
            user = userEntityRepository.findOne(id);
        }
        addContactInfo(model, user);
        addOrganisationInfo(model, user);
        model.addObject("subdivisionList", subdivisionEntityRepository.findByUser(user));
        model.addObject("licenseList", licenseEntityRepository.findByUser(user));
        return model;
    }

    private void addContactInfo(ModelAndView model, UserEntity currentUser) {
        model.addObject("email", currentUser.getEmail());
        model.addObject("phoneNumber", currentUser.getPhoneNumber());
        model.addObject("fio", currentUser.getFio());
        model.addObject("password", "");
    }

    private void addOrganisationInfo(ModelAndView model, UserEntity currentUser) {
        OrganisationEntity organisationEntity = organisationEntityRepository.findByUser(currentUser);
        if (organisationEntity == null) {
            return;
        }
        model.addObject("orgName", organisationEntity.getName());
        model.addObject("orgInn", currentUser.getInn());
        model.addObject("orgKpp", organisationEntity.getKpp());
        model.addObject("fioDir", organisationEntity.getFioDir());
        model.addObject("fioBuh", organisationEntity.getFioBuh());
        model.addObject("subjectCode", organisationEntity.getSubjectCode());
        model.addObject("orgAddress", organisationEntity.getAddress());
        model.addObject("orgEmail", organisationEntity.getEmail());
    }

    public void saveContacts(UserInfoContactDTO dto, Long id, String url, Locale locale) {
        UserEntity user;
        if (id == null) {
            user = userService.getCurrentUser();
        } else {
            user = userEntityRepository.findOne(id);
        }
        if (dto.getEmail() != null) {
            user.setEmail(dto.getEmail());
            user.setEnabled(false);

            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, locale, url));
            SecurityContextHolder.clearContext();
        }
        if (dto.getPhoneNumber() != null) {
            user.setPhoneNumber(dto.getPhoneNumber());
        }
        if (dto.getFio() != null) {
            user.setFio(dto.getFio());
        }
        userService.saveRegisteredUser(user);
    }

    public void saveOrganisation(UserInfoOrganisationDTO dto, Long id) {
        UserEntity user;
        if (id == null) {
            user = userService.getCurrentUser();
        } else {
            user = userEntityRepository.findOne(id);
        }
        OrganisationEntity organisationEntity = organisationEntityRepository.findByUser(user);
        if (organisationEntity == null) {
            organisationEntity = new OrganisationEntity();
            organisationEntity.setUser(user);
        }
        if (dto.getOrgEmail() != null) {
            organisationEntity.setEmail(dto.getOrgEmail());
        }
        if (dto.getOrgAddress() != null) {
            organisationEntity.setAddress(dto.getOrgAddress());
        }
        if (dto.getFioBuh() != null) {
            organisationEntity.setFioBuh(dto.getFioBuh());
        }
        if (dto.getFioDir() != null) {
            organisationEntity.setFioDir(dto.getFioDir());
        }
        if (dto.getOrgInn() != null) {
            user.setInn(dto.getOrgInn());
            userEntityRepository.save(user);
        }
        if (dto.getOrgKpp() != null) {
            organisationEntity.setKpp(dto.getOrgKpp());
        }
        if (dto.getOrgName() != null) {
            organisationEntity.setName(dto.getOrgName());
        }
        if (dto.getSubjectCode() != null) {
            organisationEntity.setSubjectCode(dto.getSubjectCode());
        }
        organisationEntityRepository.save(organisationEntity);
    }


}
