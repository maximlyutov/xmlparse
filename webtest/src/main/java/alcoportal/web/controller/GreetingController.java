package alcoportal.web.controller;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.services.revise.ReviseService;
import alcoportal.services.validation.ValidateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class GreetingController extends BaseContoller {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ValidateService validateService;

    @Autowired
    ReviseService reviseService;

    @RequestMapping("/")
    public String root() {
        return "redirect:index";
    }

    /**
     * Home page.
     */
    @RequestMapping(value = "/index", produces = "text/html;charset=UTF-8")
    public Object index() {
        if (!isAuthenticated() || getCurrentUser() == null) {
            return "redirect:login";
        }
        if (!getCurrentUser().getEnabled()) {
            return "redirect:emailVerification";
        }

        ModelAndView model = new ModelAndView("index");
        model.addObject("list", validateService.getDocumentsHistory(getCurrentUser()));

        model.addObject("revises", reviseService.getRevises(getCurrentUser()));

        return model;
    }

    /**
     * User zone index.
     */
    @RequestMapping("/user/index")
    public String userIndex() {
        return "user/index";
    }

    /**
     * Login form.
     */
    @RequestMapping(value = "/login", produces = "text/html;charset=UTF-8")
    public String login() {
        return "login";
    }

    /**
     * Login form with error.
     */
    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }
}
