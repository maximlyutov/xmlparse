package alcoportal.web.controller;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.userInfo.LicenseEntity;
import alcoportal.dal.domain.entity.userInfo.SubdivisionEntity;
import alcoportal.dal.repository.LicenseEntityRepository;
import alcoportal.dal.repository.SubdivisionEntityRepository;
import alcoportal.dal.repository.UserEntityRepository;
import alcoportal.dto.userInfo.UserInfoContactDTO;
import alcoportal.dto.userInfo.UserInfoLicenseDTO;
import alcoportal.dto.userInfo.UserInfoOrganisationDTO;
import alcoportal.dto.userInfo.UserInfoSubdivisionDTO;
import alcoportal.service.UserInfoService;
import alcoportal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by good on 28.10.2015.
 */
@Controller
public class UserInfoContoller extends BaseContoller {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private SubdivisionEntityRepository subdivisionEntityRepository;

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private LicenseEntityRepository licenseEntityRepository;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/userinfo/contact", method = RequestMethod.POST)
    public String saveContacts(UserInfoContactDTO dto, final HttpServletRequest request) {
        final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        userInfoService.saveContacts(dto, null, appUrl, request.getLocale());
        return "redirect:" + getPath(request) + "/userinfo#contacts";
    }

    @RequestMapping(value = "/userinfo/organisation", method = RequestMethod.POST)
    public String saveOrganisation(UserInfoOrganisationDTO dto, final HttpServletRequest request) {
        userInfoService.saveOrganisation(dto, null);
        return "redirect:" + getPath(request) + "/userinfo#recvisits";
    }

    @RequestMapping("/userinfo")
    public Object userinfo() {
        ModelAndView model = new ModelAndView("userinfo");
        return userInfoService.getModelForUserInfo(model, null);
    }

    @RequestMapping("/userinfo/license")
    public Object license() {
        ModelAndView model = new ModelAndView("userinfo/license");
        model.addObject("subdivisionList", subdivisionEntityRepository.findByUser(getCurrentUser()));
        return model;
    }

    @RequestMapping(value = "/userinfo/license", method = RequestMethod.POST)
    public Object saveLicense(@RequestParam(required = false) Long id, UserInfoLicenseDTO dto, final HttpServletRequest request) {
        LicenseEntity licenseEntity;

        if (id == null) {
            licenseEntity = new LicenseEntity();
        } else {
            licenseEntity = licenseEntityRepository.findOne(id);
        }

        licenseEntity.setUser(getCurrentUser());
        licenseEntity.setDateEnd(dto.getDateEnd());
        licenseEntity.setDateStart(dto.getDateStart());
        licenseEntity.setNumber(dto.getNumber());
        licenseEntity.setNumberBlank(dto.getNumberBlank());
        licenseEntity.setWorkType(dto.getWorkType());
        licenseEntity.setSubdivision(subdivisionEntityRepository.findOne(dto.getSubdivisionId()));
        licenseEntityRepository.save(licenseEntity);

        return "redirect:" + getPath(request) + "/userinfo#license";
    }

    @RequestMapping("userinfo/subdivision")
    public Object subdivision(Long userId, String redirectUrl) {
        ModelAndView model = new ModelAndView("userinfo/subdivision");
        model.addObject("userid", userId);
        model.addObject("redirectUrl", redirectUrl);
        return model;
    }

    @RequestMapping(value = "/userinfo/subdivision", method = RequestMethod.POST)
    public Object saveSubdivision(@RequestParam(required = false) Long id, UserInfoSubdivisionDTO dto,
                                  @RequestParam(required = false) Long userId, String redirectUrl, final HttpServletRequest request) {
        SubdivisionEntity subdivisionEntity;

        if (id == null) {
            subdivisionEntity = new SubdivisionEntity();
        } else {
            subdivisionEntity = subdivisionEntityRepository.findOne(id);
        }

        subdivisionEntity.setAddress(dto.getAddress());
        subdivisionEntity.setKpp(Long.parseLong(dto.getSdKpp()));
        subdivisionEntity.setSubjectCode(dto.getSubjectCode());
        UserEntity user;
        if (userId != null) {
            user = userEntityRepository.findOne(userId);
        } else {
            user = getCurrentUser();
            userId = user.getId();
        }
        subdivisionEntity.setUser(user);
        subdivisionEntityRepository.save(subdivisionEntity);
        return "redirect:" + getPath(request) + "/userinfo?userId=" + userId + "&redirectUrl=" + redirectUrl + "#subdivision";
    }

    @RequestMapping(value = "/userinfo/changePassword", method = RequestMethod.POST)
    public Object changePassword(@RequestParam String password, final HttpServletRequest request) {
        userService.changeUserPassword(getCurrentUser(), password);
        return "redirect:" + getPath(request) + "/userinfo#contacts";
    }
}
