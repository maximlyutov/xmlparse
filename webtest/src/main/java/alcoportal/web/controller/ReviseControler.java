package alcoportal.web.controller;

import alcoportal.services.revise.ReviseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by vladimir akummail@gmail.com on 10/22/15.
 */
@Controller
public class ReviseControler extends BaseContoller {

    @Autowired
    private ReviseService reviseService;

    @RequestMapping(value = "/revise/build/{id}", method = RequestMethod.POST)
    public Object reviseDocument(@PathVariable Long id, boolean revice, boolean choosedRevice) {
        reviseService.fullRevise(id, revice, getCurrentUser());
        return "redirect:index";
    }


    @RequestMapping(value = "/revise/delete/{id}", method = RequestMethod.POST)
    public Object reviseDocument(@PathVariable Long id) {
        reviseService.removeRevise(id, getCurrentUser());
        return "redirect:index";
    }
}
