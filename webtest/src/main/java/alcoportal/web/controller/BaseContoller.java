package alcoportal.web.controller;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by good on 21.10.2015.
 */
@Controller
public class BaseContoller {

    @Autowired
    private UserService userService;

    final Logger LOGGER = LoggerFactory.getLogger(getClass());

    UserEntity getCurrentUser() {
        return userService.getCurrentUser();
    }

    Boolean isAuthenticated() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return false;
        }
        return auth.isAuthenticated();
    }

    String getPath(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
