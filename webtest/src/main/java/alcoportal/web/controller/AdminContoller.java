package alcoportal.web.controller;

import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.ValidationErrorEntity;
import alcoportal.dal.domain.entity.userInfo.LicenseEntity;
import alcoportal.dal.domain.entity.userInfo.SubdivisionEntity;
import alcoportal.dal.repository.LicenseEntityRepository;
import alcoportal.dal.repository.SubdivisionEntityRepository;
import alcoportal.dal.repository.UserEntityRepository;
import alcoportal.dal.repository.ValidationErrorRepository;
import alcoportal.dto.admin.ValidationErrorAdminDTO;
import alcoportal.dto.userInfo.UserInfoContactDTO;
import alcoportal.dto.userInfo.UserInfoLicenseDTO;
import alcoportal.dto.userInfo.UserInfoOrganisationDTO;
import alcoportal.dto.userInfo.UserInfoSubdivisionDTO;
import alcoportal.service.UserInfoService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by good on 01.11.2015.
 */
@Controller
public class AdminContoller extends BaseContoller {

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private ValidationErrorRepository validationErrorRepository;

    @Autowired
    private SubdivisionEntityRepository subdivisionEntityRepository;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private LicenseEntityRepository licenseEntityRepository;

    @RequestMapping("/admin/index")
    public ModelAndView adminIndex() {
        ModelAndView model = new ModelAndView("admin");
        model.addObject("list", userEntityRepository.findAll());
        return model;
    }

    @RequestMapping("/admin/license")
    public Object license(Long userId, String redirectUrl) {
        ModelAndView model = new ModelAndView("admin/license");

        UserEntity user;
        if (userId == null) {
            user = getCurrentUser();
        } else {
            user = userEntityRepository.findOne(userId);
        }
        model.addObject("subdivisionList", subdivisionEntityRepository.findByUser(user));
        model.addObject("userId", userId);
        model.addObject("redirectUrl", redirectUrl);
        return model;
    }

    @RequestMapping(value = "/admin/license", method = RequestMethod.POST)
    public Object saveLicense(@RequestParam(required = false) Long id, Long userId, UserInfoLicenseDTO dto, final HttpServletRequest request) {
        LicenseEntity licenseEntity;

        if (id == null) {
            licenseEntity = new LicenseEntity();
        } else {
            licenseEntity = licenseEntityRepository.findOne(id);
        }

        UserEntity user;
        if (userId != null) {
            user = userEntityRepository.findOne(userId);
        } else {
            user = getCurrentUser();
            userId = user.getId();
        }

        licenseEntity.setUser(user);
        licenseEntity.setDateEnd(dto.getDateEnd());
        licenseEntity.setDateStart(dto.getDateStart());
        licenseEntity.setNumber(dto.getNumber());
        licenseEntity.setNumberBlank(dto.getNumberBlank());
        licenseEntity.setWorkType(dto.getWorkType());
        licenseEntity.setSubdivision(subdivisionEntityRepository.findOne(dto.getSubdivisionId()));
        licenseEntityRepository.save(licenseEntity);

        return "redirect:" + getPath(request) + "/admin/user?id=" + userId + "#license";
    }

    @RequestMapping("/admin/subdivision")
    public Object subdivision(Long userId, String redirectUrl) {
        ModelAndView model = new ModelAndView("admin/subdivision");
        model.addObject("userId", userId);
        model.addObject("redirectUrl", redirectUrl);
        return model;
    }

    @RequestMapping(value = "/admin/subdivision", method = RequestMethod.POST)
    public Object saveSubdivision(@RequestParam(required = false) Long id, final HttpServletRequest request, UserInfoSubdivisionDTO dto, @RequestParam(required = false) Long userId, String redirectUrl) {
        SubdivisionEntity subdivisionEntity;

        if (id == null) {
            subdivisionEntity = new SubdivisionEntity();
        } else {
            subdivisionEntity = subdivisionEntityRepository.findOne(id);
        }

        subdivisionEntity.setAddress(dto.getAddress());
        subdivisionEntity.setKpp(Long.parseLong(dto.getSdKpp()));
        subdivisionEntity.setSubjectCode(dto.getSubjectCode());
        UserEntity user;
        if (userId != null) {
            user = userEntityRepository.findOne(userId);
        } else {
            user = getCurrentUser();
            userId = user.getId();
        }
        subdivisionEntity.setUser(user);
        subdivisionEntityRepository.save(subdivisionEntity);
        return "redirect:" + getPath(request) + "/admin/user?id=" + userId + "&redirectUrl=" + redirectUrl + "#subdivision";
    }

    @RequestMapping("/admin/error")
    public ModelAndView error(@RequestParam Long id) {
        ModelAndView model = new ModelAndView("admin/error");
        model.addObject("error", validationErrorRepository.findOne(id));
        model.addObject("id", id);
        return model;
    }

    @RequestMapping("/admin/errors")
    public ModelAndView errors() {
        ModelAndView model = new ModelAndView("admin/errors");
        model.addObject("list", validationErrorRepository.findAll());
        return model;
    }

    @RequestMapping(value = "/admin/error", method = RequestMethod.POST)
    public String saveError(ValidationErrorAdminDTO errorDTO, @RequestParam Long id, final HttpServletRequest request) {
        ValidationErrorEntity errorEntity = validationErrorRepository.findOne(id);
        errorEntity.setDescription(errorDTO.getDescription());
        validationErrorRepository.save(errorEntity);
        return "redirect:"+getPath(request)+"/admin/errors";
    }

    @RequestMapping("/admin/kap")
    public String kap() {
        return "admin/kap";
    }

    @RequestMapping("/admin/kaps")
    public String kaps() {
        return "admin/kaps";
    }

    @RequestMapping("/admin/user")
    public ModelAndView user(@RequestParam Long id) {
        ModelAndView model = new ModelAndView("admin/user");
        model.addObject("id", id);
        return userInfoService.getModelForUserInfo(model, id);
    }

    @RequestMapping(value = "/admin/user/contacts", method = RequestMethod.POST)
    public String saveContacts(UserInfoContactDTO dto, @RequestParam Long id, final HttpServletRequest request) {
        final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        userInfoService.saveContacts(dto, null, appUrl, request.getLocale());
        return "redirect:" + getPath(request) + "/admin/user?id=" + id + "#contacts";
    }

    @RequestMapping(value = "/admin/user/organisation", method = RequestMethod.POST)
    public String saveOrganisation(UserInfoOrganisationDTO dto, @RequestParam Long id, final HttpServletRequest request) {
        userInfoService.saveOrganisation(dto, id);
        return "redirect:" + getPath(request) + "/admin/user?id=" + id + "#recvisits";
    }
}
