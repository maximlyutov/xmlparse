package alcoportal.web.controller;

import alcoportal.dal.domain.entity.PasswordResetTokenEntity;
import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.dal.domain.entity.VerificationTokenEntity;
import alcoportal.dal.repository.UserEntityRepository;
import alcoportal.error.EmailExistsException;
import alcoportal.error.InvalidOldPasswordException;
import alcoportal.error.UserAlreadyExistException;
import alcoportal.error.UserNotFoundException;
import alcoportal.registration.OnRegistrationCompleteEvent;
import alcoportal.dto.UserDto;
import alcoportal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by good on 22.10.2015.
 */
@Controller
public class RegistrationContoller extends BaseContoller {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private MessageSource messages;

    @Autowired
    private JavaMailSenderImpl mailSender;

    @Autowired
    private Environment env;

    @Autowired
    private UserDetailsService customUserDetailsService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String getRegistration() {
        return "registration";
    }

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
    public String getForgotPassword() {
        return "forgotPassword";
    }

    @RequestMapping(value = "/savePassword", method = RequestMethod.GET)
    public String getSavePassword() {
        return "savePassword";
    }

    @RequestMapping(value = "/emailVerification", method = RequestMethod.GET)
    public String getVerificationEmailPage() {
        return "emailVerification";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registerUserAccount(@ModelAttribute UserDto accountDto, final Model model, final HttpServletRequest request) {
        LOGGER.debug("Registering user account with information: {}", accountDto);

        final UserEntity registered = createUserAccount(accountDto);
        if (registered == null) {
            model.addAttribute("message", "KJFS:LJFDLSKJ");
            return "redirect:registration";
        }
        final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));

        return "redirect:emailVerification";
    }

    @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
    public String confirmRegistration(final Locale locale, final Model model, @RequestParam("token") final String token) {
        final VerificationTokenEntity verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            final String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            return "redirect:badUser.html?lang=" + locale.getLanguage();
        }

        final UserEntity user = verificationToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("message", messages.getMessage("auth.message.expired", null, locale));
            model.addAttribute("expired", true);
            model.addAttribute("token", token);
            return "redirect:badUser.html?lang=" + locale.getLanguage();
        }

        user.setEnabled(true);
        userService.saveRegisteredUser(user);
       // mailSender.send(constructNewUserEmail(user));
        model.addAttribute("message", messages.getMessage("message.accountVerified", null, locale));
        return "redirect:index";
    }

    private UserEntity createUserAccount(final UserDto accountDto) {
        UserEntity registered;
        try {
            registered = userService.registerNewUserAccount(accountDto);
        } catch (final EmailExistsException e) {
            return null;
        }
        return registered;
    }

    @RequestMapping(value = "/user/resetPassword", method = RequestMethod.POST)
    public String resetPassword(final HttpServletRequest request, @RequestParam("email") final String userEmail) {
        final UserEntity user = userEntityRepository.findByEmail(userEmail);
        if (user == null) {
            throw new UserNotFoundException();
        }

        final String token = UUID.randomUUID().toString();
        userService.createPasswordResetTokenForUser(user, token);
        final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        final SimpleMailMessage email = constructResetTokenEmail(appUrl, request.getLocale(), token, user);
        mailSender.send(email);
        return "redirect:" + getPath(request) + "/emailVerification";
    }

    @RequestMapping(value = "/user/changePassword", method = RequestMethod.GET)
    public String showChangePasswordPage(final Locale locale, final Model model, @RequestParam("id") final long id, @RequestParam("token") final String token) {
        final PasswordResetTokenEntity passToken = userService.getPasswordResetToken(token);
        final UserEntity user = passToken.getUser();

        final Calendar cal = Calendar.getInstance();
        if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("message", messages.getMessage("auth.message.expired", null, locale));
            return "redirect:/login.html?lang=" + locale.getLanguage();
        }

        final Authentication auth = new UsernamePasswordAuthenticationToken(user, null, customUserDetailsService.loadUserByUsername(user.getEmail()).getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        return "redirect:/savePassword.html?lang=" + locale.getLanguage();
    }

    @RequestMapping(value = "/user/savePassword", method = RequestMethod.POST)
    public String savePassword(final Locale locale, @RequestParam("password") final String password, final HttpServletRequest request) {
        final UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.changeUserPassword(user, password);
        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
        return "redirect:" + getPath(request) + "/index";
    }

    private final SimpleMailMessage constructResetTokenEmail(final String contextPath, final Locale locale, final String token, final UserEntity user) {
        final String url = contextPath + "/user/changePassword?id=" + user.getId() + "&token=" + token;
        final String message = messages.getMessage("message.resetPassword", null, locale);
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject("Reset Password");
        email.setText(message + " \r\n" + url);
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private final SimpleMailMessage constructNewUserEmail(final UserEntity user) {
        final String message = "Зарегистрирован новый пользователь.";
        final String inn = "ИНН: " + user.getInn();
        final String email = "Email: " + user.getEmail();
        final String fio = "ФИО: " + user.getFio();
        final SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(env.getProperty("support.email"));
        simpleMailMessage.setSubject("Reset Password");
        simpleMailMessage.setText(message + " \r\n" + fio + " \r\n" + email + " \r\n" + inn + " \r\n");
        simpleMailMessage.setFrom(env.getProperty("support.email"));
        return simpleMailMessage;
    }
}
