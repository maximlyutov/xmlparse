package alcoportal.web.controller;

import alcoportal.dal.domain.entity.DocumentHistoryEntity;
import alcoportal.dal.domain.entity.UserEntity;
import alcoportal.exseptions.ValidateExseption;
import alcoportal.services.validation.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by vladimir akummail@gmail.com on 10/6/15.
 */
@Controller
public class ValidationController extends BaseContoller {
    @Autowired
    private ValidateService validateService;

    @ResponseBody
    @RequestMapping(value = "/validate/load", method = RequestMethod.POST)
    public Object loadAndValidate(@RequestParam("file") MultipartFile file) throws IOException, ValidateExseption {
        if (!file.isEmpty()) {
            return validateService.validateAndSave(file.getOriginalFilename(), file.getBytes(), getCurrentUser());
        }
        return null;
    }

    //  @ResponseBody
    @RequestMapping(value = "/documenthistoru/{id}", method = RequestMethod.GET)
    public HttpEntity<byte[]> downloadDocumenthistoru(@PathVariable("id") long documenHistoryId) {

        DocumentHistoryEntity documentHistoryEntity = validateService.getDocumentHistory(getCurrentUser(), documenHistoryId);
        // get your file as InputStream
        HttpHeaders header = new HttpHeaders();

        header.setContentType(MediaType.APPLICATION_XML);
        header.set("Content-Disposition", "attachment; filename=" + documentHistoryEntity.getFileName().replace(" ", "_"));
        header.setContentLength(documentHistoryEntity.getDocument().length);
        return new HttpEntity<>(documentHistoryEntity.getDocument(), header);
    }
}
