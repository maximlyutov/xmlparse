package alcoportal.web.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.stereotype.Component;
import java.text.SimpleDateFormat;

/**
 * Created by vladimir akummail@gmail.com on 10/25/15.
 */

@Component
public class CustomObjectMapper extends ObjectMapper {

    public CustomObjectMapper() {
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);
        setDateFormat(DATE_FORMATTER);
    }
}